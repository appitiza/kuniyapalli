package net.appitiza.config


object Config {
    //staging
    const val BASEURL = "COLLECTION_DATA_TEST"
    const val IS_MARKET_VERSION = false
    const val IS_LOGG_ENABLED = true
}
