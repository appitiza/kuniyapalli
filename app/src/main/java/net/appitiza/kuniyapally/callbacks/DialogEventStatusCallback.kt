package net.appitiza.kuniyapally.callbacks

interface DialogEventStatusCallback {
    fun onCollectedClick()
    fun onUncollectedClick()
}