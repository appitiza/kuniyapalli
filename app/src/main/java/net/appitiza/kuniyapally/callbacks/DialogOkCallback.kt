package net.appitiza.kuniyapally.callbacks

interface DialogOkCallback {
    fun okClick()
}