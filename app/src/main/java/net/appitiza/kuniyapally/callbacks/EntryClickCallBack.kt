package net.appitiza.kuniyapally.callbacks

import android.view.View

interface EntryClickCallBack {

    fun onEntryClick(view: View,position: Int)
    fun onDeleteClick(view: View, position: Int)
}