package net.appitiza.kuniyapally.callbacks

interface DialogSettingsCallback {

    fun graphClick()
    fun changePasswordClick()
    fun logout()
}