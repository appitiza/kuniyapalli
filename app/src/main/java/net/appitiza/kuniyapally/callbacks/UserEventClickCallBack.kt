package net.appitiza.kuniyapally.callbacks

import android.view.View

interface UserEventClickCallBack {

    fun onEntryClick(status:String,position: Int)
}