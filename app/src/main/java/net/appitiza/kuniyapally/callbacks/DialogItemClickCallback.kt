package net.appitiza.kuniyapally.callbacks

interface DialogItemClickCallback {
    fun itemClick(text:String)
}