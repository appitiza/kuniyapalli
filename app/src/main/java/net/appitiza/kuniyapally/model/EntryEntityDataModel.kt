package net.appitiza.kuniyapally.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import net.appitiza.kuniyapally.model.EntryDataModel
import java.io.Serializable


@Entity
class EntryEntityDataModel(

    @ColumnInfo(name = "servercode") var servercode: String,
    @ColumnInfo(name = "code") var code: String,
    @ColumnInfo(name = "owner") var owner: String,
    @ColumnInfo(name = "profession") var profession: String,
    @ColumnInfo(name = "area") var area: String,
    @ColumnInfo(name = "housename") var housename: String,
    @ColumnInfo(name = "address") var address: String,
    @ColumnInfo(name = "contact") var contact: String,
    @ColumnInfo(name = "whatsapp") var whatsapp: String,
    @ColumnInfo(name = "addedby") var addedby: String
) : Serializable {
    @PrimaryKey(autoGenerate = true)
    var entryid = 0

    constructor(
        entryid: Int,
        servercode: String,
        code: String,
        owner: String,
        profession: String,
        area: String,
        housename: String,
        address: String,
        contact: String,
        whatsapp: String,
        addedby: String
    ) : this(
        servercode,
        code,
        owner,
        profession,
        area,
        housename,
        address,
        contact,
        whatsapp,
        addedby
    ) {

        this.entryid = entryid
    }

    fun toEntry(): EntryDataModel {
        val entry = EntryDataModel(
            this.entryid,
            this.servercode,
            this.code,
            this.owner,
            this.profession,
            this.area,
            this.housename,
            this.address,
            this.contact,
            this.whatsapp,
            this.addedby
        )
        return entry
    }
}