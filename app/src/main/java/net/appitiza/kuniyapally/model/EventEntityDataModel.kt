package net.appitiza.kuniyapally.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.PrimaryKey
import java.io.Serializable


@Entity
class EventEntityDataModel(
    @ColumnInfo(name = "entryData") var entryData: String,
    @ColumnInfo(name = "fEntryId") var fEntryId: Int
) : Serializable {

    @PrimaryKey(autoGenerate = true)
    var eventId = 0

    constructor(
        eventId: Int,
        entryData: String,
        fEntryId: Int
    ) : this(
        entryData,
        fEntryId
    ) {

        this.eventId = eventId
    }


}