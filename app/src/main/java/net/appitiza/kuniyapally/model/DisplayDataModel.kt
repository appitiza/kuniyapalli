package net.appitiza.kuniyapally.model

import java.io.Serializable

class DisplayDataModel(
    entryid: Int,
    servercode: String,
    alphabet: String,
    code: String,
    owner: String,
    profession: String,
    area: String,
    house: String,
    contact: String,
    address: String,
    isWhatsAppAvailable: Boolean,
    addedby: String
) : Serializable {
    var entryid = entryid
    var servercode = servercode
    var alphabet = alphabet
    var code = code
    var owner = owner
    var profession = profession
    var area = area
    var house = house
    var contact = contact
    var address = address
    var isWhatsAppAvailable = isWhatsAppAvailable
    var addedby = addedby

}