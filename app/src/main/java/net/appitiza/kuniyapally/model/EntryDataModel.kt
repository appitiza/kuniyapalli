package net.appitiza.kuniyapally.model

import java.io.Serializable


class EntryDataModel : Serializable {

    var entryid: Int = 0
    var servercode: String = ""
    var code: String = ""
    var owner: String = ""
    var profession: String = ""
    var area: String = ""
    var housename: String = ""
    var address: String = ""
    var contact: String = ""
    var whatsapp: String = ""
    var addedby: String = ""

    constructor(
        entryid: Int,
        servercode: String,
        code: String,
        owner: String,
        profession: String,
        area: String,
        housename: String,
        address: String,
        contact: String,
        whatsapp: String,
        addedby: String
    ) {

        this.entryid = entryid
        this.servercode = servercode
        this.code = code
        this.owner = owner
        this.profession = profession
        this.area = area
        this.housename = housename
        this.address = address
        this.contact = contact
        this.whatsapp = whatsapp
        this.addedby = addedby
    }

    fun toDisplayEntry(): DisplayDataModel {

        val alphabet: String = if(this.owner.isNotEmpty()){this.owner.substring(0, 1).toUpperCase()}
        else
        {
            ""
        }
        val isWhatsappAvailable = !this.whatsapp.isEmpty()
        val data = DisplayDataModel(
            this.entryid,
            this.servercode,
            alphabet,
            this.code,
            this.owner,
            this.profession,
            this.area,
            this.housename,
            this.contact,
            this.address,
            isWhatsappAvailable,
            this.addedby
        )
        return data
    }
}