package net.appitiza.kuniyapally.model

import androidx.room.*

@Dao
interface EntryDAO {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(entryEntity: EntryEntityDataModel?): Long

    @Query("DELETE FROM EntryEntityDataModel WHERE servercode = :servercode")
    suspend fun deleteItem(servercode: String)

    @Update
    suspend fun update(entryEntity: EntryEntityDataModel?)

    @Query("SELECT * FROM EntryEntityDataModel")
    suspend fun getAll(): List<EntryEntityDataModel>

    @Query("SELECT * FROM EntryEntityDataModel where owner LIKE '%' || :owner || '%' or code LIKE '%' || :owner || '%'")
    suspend fun search(owner: String): List<EntryEntityDataModel>


    @Query("SELECT * FROM EntryEntityDataModel where servercode = :servercode")
    suspend fun getEntryDetails(servercode: String): EntryEntityDataModel


    @Query("SELECT * FROM EventEntityDataModel where fEntryId = :entryID")
    suspend fun getUserEventStatus(entryID: Int): EventEntityDataModel

    @Query("DELETE FROM EventEntityDataModel")
    suspend fun deleteAllEvent()


    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(entryEntity: EventEntityDataModel?): Long

    @Update
    suspend fun update(entryEntity: EventEntityDataModel?)
}