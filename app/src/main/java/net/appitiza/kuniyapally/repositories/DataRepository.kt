package net.appitiza.kuniyapally.repositories

import net.appitiza.kuniyapally.model.EntryEntityDataModel
import net.appitiza.kuniyapally.model.EventEntityDataModel

interface DataRepository {

    suspend fun emailLogin(email:String,password:String):Boolean

    suspend fun logOut():Boolean

    suspend fun resetPassword(email:String):Boolean

    suspend fun guestLogin(phone:String):Boolean

    suspend fun addData(entryEntity: EntryEntityDataModel):Boolean

    suspend fun editData(entryEntity: EntryEntityDataModel):Boolean
    suspend fun deleteDataItem(serverCode: String):Boolean
    suspend fun getLocalDataList(): List<EntryEntityDataModel>

    suspend fun getUserEvent(entryId: Int): EventEntityDataModel

    suspend fun getRemoteDataList(): List<EntryEntityDataModel>

    suspend fun search(fullName: String): List<EntryEntityDataModel>


    suspend fun getDataDetails(servercode: String): EntryEntityDataModel


    suspend fun addEventData(entryEntity: EventEntityDataModel):Boolean



    suspend fun deleteEventData():Boolean
}