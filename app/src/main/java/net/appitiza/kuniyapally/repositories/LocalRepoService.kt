package net.appitiza.kuniyapally.repositories

import android.content.Context
import net.appitiza.kuniyapally.app.BaseApp
import net.appitiza.kuniyapally.constants.Constants
import net.appitiza.kuniyapally.model.EntryEntityDataModel
import net.appitiza.kuniyapally.model.EventEntityDataModel
import net.appitiza.kuniyapally.utils.PreferenceHelper

class LocalRepoService(val context: Context) : DataRepository {
    private var mLoggedInAsAdmin by PreferenceHelper(Constants.PREF_LOGGEDIN_AS_AdMIN, false)
    private var mLoggedInAsGuest by PreferenceHelper(Constants.PREF_LOGGEDIN_AS_GUEST, false)
    private var mLoggedInUserEmail by PreferenceHelper(Constants.PREF_LOGGEDIN_USER_EMAIL, "")
    private var mLoggedInUserPassword by PreferenceHelper(Constants.PREF_LOGGEDIN_USER_PASSWORD, "")
    private var mLoggedIn by PreferenceHelper(Constants.PREF_LOGGEDIN, false)

    override suspend fun emailLogin(email: String, password: String): Boolean {

        return false
    }

    override suspend fun logOut():Boolean {
        mLoggedIn = false
        mLoggedInAsAdmin = false
        mLoggedInAsGuest = false
        mLoggedInUserEmail = ""
        mLoggedInUserPassword = ""
        return true
    }

    override suspend fun guestLogin(phone: String): Boolean {
        return false
    }

    override suspend fun resetPassword(email: String): Boolean {
        return false
    }

    override suspend fun addData(entryEntity: EntryEntityDataModel): Boolean {
        return false
    }


    override suspend fun editData(entryEntity: EntryEntityDataModel): Boolean {
        return false
    }

    override suspend fun getLocalDataList(): List<EntryEntityDataModel> {
        return (context.applicationContext as BaseApp).databaseInstance()!!.entryDao()
            .getAll()
    }



    override suspend fun getRemoteDataList(): List<EntryEntityDataModel> {
        return (context.applicationContext as BaseApp).databaseInstance()!!.entryDao()
            .getAll()
    }

    override suspend fun search(fullName: String): List<EntryEntityDataModel> {
        return (context.applicationContext as BaseApp).databaseInstance()!!.entryDao()
            .search(fullName)
    }



    override suspend fun deleteDataItem(servercode: String): Boolean {

        return false
    }

    override suspend fun getDataDetails(servercode: String): EntryEntityDataModel {
        return (context.applicationContext as BaseApp).databaseInstance()!!.entryDao()
            .getEntryDetails(servercode)
    }



    override suspend fun getUserEvent(entryId: Int): EventEntityDataModel {
        return (context.applicationContext as BaseApp).databaseInstance()!!.entryDao()
            .getUserEventStatus(entryId)
    }

    override suspend fun addEventData(entryEntity: EventEntityDataModel): Boolean {

        //insert to DB
        val data = getUserEvent(entryEntity.fEntryId)
        if (data == null ){
            (context.applicationContext as BaseApp).databaseInstance()?.entryDao()
                ?.insert(entryEntity = entryEntity)!!.toInt()
        }
        else
        {
            (context.applicationContext as BaseApp).databaseInstance()?.entryDao()
                ?.update(entryEntity = entryEntity)
        }

        return true

    }

    override suspend fun deleteEventData(): Boolean {
        (context.applicationContext as BaseApp).databaseInstance()?.entryDao()
            ?.deleteAllEvent()
        return true
    }

}