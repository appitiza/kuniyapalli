package net.appitiza.kuniyapally.repositories

import android.content.Context
import android.util.Log
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FieldValue
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.FirebaseFirestoreException
import com.google.firebase.firestore.Query
import kotlinx.coroutines.tasks.await
import net.appitiza.config.Config
import net.appitiza.kuniyapally.app.BaseApp
import net.appitiza.kuniyapally.constants.Constants
import net.appitiza.kuniyapally.model.EntryEntityDataModel
import net.appitiza.kuniyapally.model.EventEntityDataModel
import net.appitiza.kuniyapally.utils.PreferenceHelper


class RemoteRepoService(val context: Context) : DataRepository {
    private var mLastUpdatedTime by PreferenceHelper(Constants.PREF_LAST_UPDATED_TIME, "0")
    private var mLoggedInAsAdmin by PreferenceHelper(Constants.PREF_LOGGEDIN_AS_AdMIN, false)
    private var mLoggedInAsGuest by PreferenceHelper(Constants.PREF_LOGGEDIN_AS_GUEST, false)
    private var mLoggedInUserEmail by PreferenceHelper(Constants.PREF_LOGGEDIN_USER_EMAIL, "")
    private var mLoggedInUserPassword by PreferenceHelper(Constants.PREF_LOGGEDIN_USER_PASSWORD, "")
    private var mLoggedIn by PreferenceHelper(Constants.PREF_LOGGEDIN, false)

    private var mAuth: FirebaseAuth? = null
    override suspend fun emailLogin(email: String, password: String): Boolean {

        return try {
            mAuth = FirebaseAuth.getInstance()
            val result = mAuth?.signInWithEmailAndPassword(email, password)?.await()
            return if (mAuth?.currentUser != null && mAuth?.currentUser!!.email == email) {
                mLoggedIn = true
                mLoggedInAsAdmin = true
                mLoggedInAsGuest = false
                mLoggedInUserEmail = mAuth?.currentUser!!.email.toString()
                mLoggedInUserPassword = password
                true
            } else {
                mLoggedIn = false
                mLoggedInAsAdmin = false
                mLoggedInAsGuest = false
                mLoggedInUserEmail = ""
                mLoggedInUserPassword = ""
                false
            }
        } catch (e: Exception) {
            false
        }


    }

    override suspend fun logOut(): Boolean {
        mLoggedIn = false
        mLoggedInAsAdmin = false
        mLoggedInAsGuest = false
        mLoggedInUserEmail = ""
        mLoggedInUserPassword = ""
        return true
    }

    override suspend fun resetPassword(email: String): Boolean {
        return try {
            mAuth = FirebaseAuth.getInstance()
            val result = mAuth?.sendPasswordResetEmail(email)?.await()
            true
        } catch (e: Exception) {
            false
        }


    }

    override suspend fun guestLogin(phone: String): Boolean {
        return try {
            val db = FirebaseFirestore.getInstance()
            val result = db.collection(Config.BASEURL)
                .whereEqualTo(Constants.contact, phone)
                .get()
                .await()

            return if (result.documents.isNotEmpty()) {
                mLoggedInAsAdmin = false
                mLoggedInAsGuest = true
                true
            } else {
                mLoggedInAsAdmin = false
                mLoggedInAsGuest = false
                false
            }

        } catch (e: FirebaseFirestoreException) {
            false
        }
    }

    override suspend fun addData(entryEntity: EntryEntityDataModel): Boolean {

        return try {
            val db = FirebaseFirestore.getInstance()
            val map = HashMap<String, Any>()
            map[Constants.entryid] = entryEntity.entryid.toString()
            map[Constants.code] = entryEntity.code
            map[Constants.owner] = entryEntity.owner
            map[Constants.profession] = entryEntity.profession
            map[Constants.area] = entryEntity.area
            map[Constants.housename] = entryEntity.housename
            map[Constants.address] = entryEntity.address
            map[Constants.contact] = entryEntity.contact
            map[Constants.whatsapp] = entryEntity.whatsapp
            map[Constants.addedby] = entryEntity.addedby
            map[Constants.timestamp] = FieldValue.serverTimestamp()
            //map[Constants.localtimestamp] = Timestamp(System.currentTimeMillis()).toString()
            map[Constants.localtimestamp] = System.currentTimeMillis()
            map[Constants.isDeleted] = 0

            val result = db.collection(Config.BASEURL)
                .add(map)
                .await()

            //insert to DB
            entryEntity.servercode = result.id
            (context.applicationContext as BaseApp).databaseInstance()?.entryDao()
                ?.insert(entryEntity = entryEntity)!!.toInt()
            true
        } catch (e: FirebaseFirestoreException) {
            false
        }
    }

    override suspend fun editData(entryEntity: EntryEntityDataModel): Boolean {

        return try {
            //update to DB
            (context.applicationContext as BaseApp).databaseInstance()?.entryDao()
                ?.update(entryEntity = entryEntity)

            val map = HashMap<String, Any>()
            map[Constants.entryid] = entryEntity.entryid.toString()
            map[Constants.code] = entryEntity.code
            map[Constants.owner] = entryEntity.owner
            map[Constants.profession] = entryEntity.profession
            map[Constants.area] = entryEntity.area
            map[Constants.housename] = entryEntity.housename
            map[Constants.address] = entryEntity.address
            map[Constants.contact] = entryEntity.contact
            map[Constants.whatsapp] = entryEntity.whatsapp
            map[Constants.addedby] = entryEntity.addedby
            map[Constants.timestamp] = FieldValue.serverTimestamp()
            map[Constants.localtimestamp] = System.currentTimeMillis()
            map[Constants.isDeleted] = 0

            val db = FirebaseFirestore.getInstance()
            val result = db.collection(Config.BASEURL)
                .document(entryEntity.servercode)
                .update(map)
                .await()


            true
        } catch (e: FirebaseFirestoreException) {
            false
        }

    }

    override suspend fun deleteDataItem(serverCode: String): Boolean {
        return try {
            val map = HashMap<String, Any>()
            map[Constants.localtimestamp] = System.currentTimeMillis()
            map[Constants.isDeleted] = 1


            val db = FirebaseFirestore.getInstance()
            val result = db.collection(Config.BASEURL)
                .document(serverCode)
                .update(map)
                .await()

            //delete from DB
            (context.applicationContext as BaseApp).databaseInstance()?.entryDao()
                ?.deleteItem(serverCode)
            true
        } catch (e: FirebaseFirestoreException) {
            false
        }
    }

    override suspend fun getRemoteDataList(): List<EntryEntityDataModel> {
        val db = FirebaseFirestore.getInstance()
        val result = db.collection(Config.BASEURL)
            .orderBy(Constants.localtimestamp, Query.Direction.ASCENDING)
            .whereGreaterThan(Constants.localtimestamp, mLastUpdatedTime.toLong())
            .get()
            .await()
        for (document in result.documents) {
            Log.d(" data", document.id + " => " + document.data)
            val mData =
                EntryEntityDataModel(
                    (document.id),
                    (document.data?.get(Constants.code)).toString(),
                    (document.data?.get(Constants.owner)).toString(),
                    (document.data?.get(Constants.profession)).toString(),
                    (document.data?.get(Constants.area)).toString(),
                    (document.data?.get(Constants.housename)).toString(),
                    (document.data?.get(Constants.address)).toString(),
                    (document.data?.get(Constants.contact)).toString(),
                    (document.data?.get(Constants.whatsapp)).toString(),
                    (document.data?.get(Constants.addedby)).toString()

                )

            if ((document.data?.get(Constants.isDeleted)).toString() == "0") {

                val data = (context.applicationContext as BaseApp).databaseInstance()?.entryDao()
                    ?.getEntryDetails( mData.servercode)
                if(data != null)
                {
                    //update DB
                    (context.applicationContext as BaseApp).databaseInstance()?.entryDao()
                        ?.update(entryEntity = mData)
                }
                else
                {
                    (context.applicationContext as BaseApp).databaseInstance()?.entryDao()
                        ?.insert(entryEntity = mData)
                }

            } else {
                (context.applicationContext as BaseApp).databaseInstance()?.entryDao()
                    ?.deleteItem(mData.servercode)
            }


        }
        val arrayList = (context.applicationContext as BaseApp).databaseInstance()?.entryDao()
            ?.getAll()
        mLastUpdatedTime = System.currentTimeMillis().toString()

        return arrayList!!
    }

    override suspend fun getLocalDataList(): List<EntryEntityDataModel> {
        return (context.applicationContext as BaseApp).databaseInstance()!!.entryDao()
            .getAll()
    }


    override suspend fun search(fullName: String): List<EntryEntityDataModel> {
        return (context.applicationContext as BaseApp).databaseInstance()!!.entryDao()
            .search(fullName)
    }


    override suspend fun getDataDetails(servercode: String): EntryEntityDataModel {
        return (context.applicationContext as BaseApp).databaseInstance()!!.entryDao()
            .getEntryDetails(servercode)
    }


    override suspend fun getUserEvent(entryId: Int): EventEntityDataModel {
        return (context.applicationContext as BaseApp).databaseInstance()!!.entryDao()
            .getUserEventStatus(entryId)
    }

    override suspend fun addEventData(entryEntity: EventEntityDataModel): Boolean {

        //insert to DB
        val data = getUserEvent(entryEntity.fEntryId)
        if (data == null) {
            (context.applicationContext as BaseApp).databaseInstance()?.entryDao()
                ?.insert(entryEntity = entryEntity)!!.toInt()
        } else {
            (context.applicationContext as BaseApp).databaseInstance()?.entryDao()
                ?.update(entryEntity = entryEntity)
        }

        return true

    }

    override suspend fun deleteEventData(): Boolean {
        (context.applicationContext as BaseApp).databaseInstance()?.entryDao()
            ?.deleteAllEvent()
        return true
    }


}