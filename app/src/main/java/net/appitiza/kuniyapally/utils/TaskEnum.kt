package net.appitiza.kuniyapally.utils

enum class TaskEnum(val intValue: Int) {


    PDF_GENERATION(0),
    FETCH_DATA(1),
    ADD_DATA(2),
    EDIT_DATA(3),
    SEARCH_DATA(4),
    EMAIL_LOGIN(5),
    RESET_PASSWORD(6),
    GUEST_LOGIN(7),
    FETCH_EVENT(8),
    UPDATE_EVENT(9),
    LOGOUT(10)
}
