package net.appitiza.kuniyapally.utils

sealed class OperationStatus() {
    class Processing(val message: String, val task: TaskEnum) : OperationStatus()
    class Failed(val message: String, val task: TaskEnum) : OperationStatus()
    class Success(val message: String, val task: TaskEnum) : OperationStatus()
    class ValidationFailed(val message: String, val task: TaskEnum) : OperationStatus()
}