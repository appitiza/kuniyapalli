package net.appitiza.kuniyapally.utils

import android.app.Dialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.view.Gravity
import android.view.Window
import android.view.WindowManager
import android.widget.Button
import android.widget.TextView
import androidx.core.content.ContextCompat
import net.appitiza.kuniyapally.R
import net.appitiza.kuniyapally.callbacks.DialogEventStatusCallback
import net.appitiza.kuniyapally.callbacks.DialogItemClickCallback
import net.appitiza.kuniyapally.callbacks.DialogOkCallback
import net.appitiza.kuniyapally.callbacks.DialogSettingsCallback

class DialogUtils(val context: Context) {
    private lateinit var infoDialog: Dialog
    fun showOkDialog(message: String, callback: DialogOkCallback) {
        if(::infoDialog.isInitialized && infoDialog != null && infoDialog.isShowing)
        {
            infoDialog.dismiss()
        }
        infoDialog = createCustomDialog(
            context, R.layout.dialog_ok,
            R.style.PopDialogAnimation, isBlureBehind = true, cancelable = true, isBottom = true
        )

        infoDialog!!.findViewById<TextView>(R.id.tvContent).text = message
        infoDialog.findViewById<Button>(R.id.btnOk).setOnClickListener {
            infoDialog.dismiss()
            callback.okClick()


        }

        infoDialog.show()
    }

    fun showEventStatusDialog(message: String, callback: DialogEventStatusCallback) {

        if(::infoDialog.isInitialized && infoDialog != null && infoDialog.isShowing)
        {
            infoDialog.dismiss()
        }

        infoDialog = createCustomDialog(
            context, R.layout.dialog_event,
            R.style.PopDialogAnimation, isBlureBehind = true, cancelable = true, isBottom = true
        )

        infoDialog!!.findViewById<TextView>(R.id.tvContent).text = message

        if (message == context.getString(R.string.collected)) {
            infoDialog!!.findViewById<TextView>(R.id.tvContent)
                .setTextColor(ContextCompat.getColor(context, R.color.colorPrimary))
            infoDialog.findViewById<Button>(R.id.btnChange).text =
                context.getString(R.string.uncollected)
        } else {
            infoDialog!!.findViewById<TextView>(R.id.tvContent)
                .setTextColor(ContextCompat.getColor(context, R.color.delete_bg_color))
            infoDialog.findViewById<Button>(R.id.btnChange).text =
                context.getString(R.string.collected)
        }
        infoDialog.findViewById<Button>(R.id.btnChange).setOnClickListener {
            infoDialog.dismiss()
            if (message == context.getString(R.string.collected)) {
                callback.onUncollectedClick()
            } else {
                callback.onCollectedClick()
            }


        }

        infoDialog.show()
    }

    fun showProfessionDialog(callback: DialogItemClickCallback) {

        if(::infoDialog.isInitialized && infoDialog != null && infoDialog.isShowing)
        {
            infoDialog.dismiss()
        }

        infoDialog = createCustomDialog(
            context, R.layout.dialog_profession,
            R.style.PopDialogAnimation, isBlureBehind = true, cancelable = true, isBottom = true
        )

        infoDialog!!.findViewById<TextView>(R.id.tvAbroad).setOnClickListener {
            infoDialog.dismiss()
            callback.itemClick(context.getString(R.string.abroad))
        }
        infoDialog.findViewById<TextView>(R.id.tvEngineer).setOnClickListener {
            infoDialog.dismiss()
            callback.itemClick(context.getString(R.string.engineer))
        }
        infoDialog.findViewById<TextView>(R.id.tvDoctor).setOnClickListener {
            infoDialog.dismiss()
            callback.itemClick(context.getString(R.string.doctor))
        }
        infoDialog.findViewById<TextView>(R.id.tvLabour).setOnClickListener {
            infoDialog.dismiss()
            callback.itemClick(context.getString(R.string.labour))
        }
        infoDialog.findViewById<TextView>(R.id.tvSelf).setOnClickListener {
            infoDialog.dismiss()
            callback.itemClick(context.getString(R.string.self_employee))
        }
        infoDialog.findViewById<TextView>(R.id.tvBusiness).setOnClickListener {
            infoDialog.dismiss()
            callback.itemClick(context.getString(R.string.business))
        }
        infoDialog.findViewById<TextView>(R.id.tvGovernment).setOnClickListener {
            infoDialog.dismiss()
            callback.itemClick(context.getString(R.string.gov_employee))
        }

        infoDialog.show()
    }

    fun showAreaDialog(callback: DialogItemClickCallback) {

        if(::infoDialog.isInitialized && infoDialog != null && infoDialog.isShowing)
        {
            infoDialog.dismiss()
        }

        infoDialog = createCustomDialog(
            context, R.layout.dialog_area,
            R.style.PopDialogAnimation, isBlureBehind = true, cancelable = true, isBottom = true
        )

        infoDialog!!.findViewById<TextView>(R.id.tvSouth).setOnClickListener {
            infoDialog.dismiss()
            callback.itemClick(context.getString(R.string.south))
        }
        infoDialog.findViewById<TextView>(R.id.tvNorth).setOnClickListener {
            infoDialog.dismiss()
            callback.itemClick(context.getString(R.string.north))
        }


        infoDialog.show()
    }

    fun showSettingDialog(callback: DialogSettingsCallback) {

        if(::infoDialog.isInitialized && infoDialog != null && infoDialog.isShowing)
        {
            infoDialog.dismiss()
        }

        infoDialog = createCustomDialog(
            context, R.layout.dialog_settings,
            R.style.PopDialogAnimation, isBlureBehind = true, cancelable = true, isBottom = true
        )


        infoDialog!!.findViewById<TextView>(R.id.tvGraph).setOnClickListener {
            infoDialog.dismiss()
            callback.graphClick()
        }
        infoDialog.findViewById<TextView>(R.id.tvChangePassword).setOnClickListener {
            infoDialog.dismiss()
            callback.changePasswordClick()
        }

        infoDialog.findViewById<TextView>(R.id.tvLogout).setOnClickListener {
            infoDialog.dismiss()
            callback.logout()
        }

        infoDialog.show()
    }

    fun showDialog(message: String) {

        if(::infoDialog.isInitialized && infoDialog != null && infoDialog.isShowing)
        {
            infoDialog.dismiss()
        }

        infoDialog = createCustomDialog(
            context, R.layout.dialog_ok,
            R.style.PopDialogAnimation, isBlureBehind = true, cancelable = true, isBottom = true
        )

        infoDialog!!.findViewById<TextView>(R.id.tvContent).text = message
        infoDialog.findViewById<Button>(R.id.btnOk).setOnClickListener {
            infoDialog.dismiss()


        }

        infoDialog.show()
    }


    private fun createCustomDialog(
        context: Context,
        layoutId: Int,
        animationStyleId: Int,
        isBlureBehind: Boolean,
        cancelable: Boolean?,
        isBottom: Boolean
    ): Dialog {

        val dialog = Dialog(
            context,
            0
        )

        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)

        dialog.setContentView(layoutId)

        dialog.window!!.attributes.windowAnimations = animationStyleId
        dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

        if (isBottom) {

            val lp = dialog.window!!.attributes
            lp.gravity = Gravity.BOTTOM
            lp.width = WindowManager.LayoutParams.MATCH_PARENT
            lp.height = WindowManager.LayoutParams.MATCH_PARENT
            lp.dimAmount = 0.7f
            dialog.window!!.attributes = lp

        }



        if (isBlureBehind)
            dialog.window!!.attributes.dimAmount = 0.8f

        dialog.setCancelable(cancelable!!)

        return dialog

    }
}