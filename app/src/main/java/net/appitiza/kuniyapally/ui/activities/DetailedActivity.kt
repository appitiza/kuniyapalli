package net.appitiza.kuniyapally.ui.activities

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.provider.Settings
import android.view.View
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.activity_detailed.*
import net.appitiza.kuniyapally.R
import net.appitiza.kuniyapally.constants.Constants
import net.appitiza.kuniyapally.model.EntryDataModel
import net.appitiza.kuniyapally.utils.OperationStatus
import net.appitiza.kuniyapally.utils.PreferenceHelper
import net.appitiza.kuniyapally.viewmodels.detailed.DetailedViewModel
import net.appitiza.kuniyapally.viewmodels.detailed.DetailedViewModelFactory

class DetailedActivity : BaseActivity() {
    private lateinit var mViewModel: DetailedViewModel
    private var mServerCode: String = ""

    private var mLoggedInAsAdmin by PreferenceHelper(Constants.PREF_LOGGEDIN_AS_AdMIN, false)
    private lateinit var mData: EntryDataModel
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detailed)
        showToolbar()
        showBack()
        hideSettings()
        hideEvents()
        if (mLoggedInAsAdmin) {
            showOption()
        } else {
            hideOption()
        }
        initialize()
        setClick()
    }

    override fun onStart() {
        LocalBroadcastManager.getInstance(this)
            .registerReceiver(
                listenerEdit,
                IntentFilter(Constants.ACTION_BROADCAST_EDIT)
            )
        super.onStart()

    }

    private fun initialize() {
        getOptionButton().setImageResource(R.drawable.pencil)
        mServerCode = intent.getStringExtra("servercode").toString()

        mViewModel =
            ViewModelProvider(
                this,
                DetailedViewModelFactory(this)
            ).get(DetailedViewModel::class.java)


        mViewModel.getEntryLiveData().observe(
            this,
            Observer<EntryDataModel> { entrymodel ->
                this.run {
                    mData = entrymodel
                    setDetails(mData)

                }
            })

        mViewModel.getStatus().observe(
            this,
            Observer<OperationStatus> { status ->
                this.run {
                    when (status) {
                        is OperationStatus.Processing -> {
                            //display progress
                            showProgress()
                        }
                        is OperationStatus.Failed -> {
                            //dismiss progress
                            dismissProgress()
                        }
                        is OperationStatus.Success -> {
                            //dismiss progress
                            dismissProgress()
                        }

                    }

                }
            })

        mViewModel.getEntryDetails(mServerCode)
    }

    private fun setDetails(entry: EntryDataModel) {
        etCode.setText(entry.code)
        etFullName.setText(entry.owner)
        if (entry.profession.isNotEmpty()) {
            ilProfession.visibility = View.VISIBLE
            etProfession.setText(entry.profession)
        }
        if (entry.area.isNotEmpty()) {
            ilArea.visibility = View.VISIBLE
            etArea.setText(entry.area)
        }
        if (entry.housename.isNotEmpty()) {
            ilHouseName.visibility = View.VISIBLE
            etHouseName.setText(entry.housename)
        }
        if (entry.address.isNotEmpty()) {
            ilAddress.visibility = View.VISIBLE
            etAddress.setText(entry.address)
        }
        if (entry.contact.isNotEmpty()) {
            ilContact.visibility = View.VISIBLE
            btnCall.visibility = View.VISIBLE
            etContact.setText(entry.contact)
        }
        if (entry.whatsapp.isNotEmpty()) {
            ilWhatsAppNumber.visibility = View.VISIBLE
            btnWhatsApp.visibility = View.VISIBLE
            etWhatsAppNumber.setText(entry.whatsapp)
        }

    }

    private fun setClick() {
        getBackButton().setOnClickListener {
            finish()
        }
        getOptionButton().setOnClickListener {
            if (mLoggedInAsAdmin) {
                mViewModel.moveToEdit(mServerCode)
            }
        }
        btnCall.setOnClickListener {
            mViewModel.loadCall(mData.contact)
        }
        btnWhatsApp.setOnClickListener {
            mViewModel.openWatsAppIntent(mData.contact)
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        when (requestCode) {
            Constants.REQUEST_PERMISSIONSREQUEST_CALL_CODE -> {
                when {

                    grantResults.any { it != PackageManager.PERMISSION_GRANTED } -> {
                        Snackbar
                            .make(
                                etFullName,
                                getString(R.string.need_call_permission),
                                Snackbar.LENGTH_LONG
                            )
                            .setAction(R.string.ok) {

                                startActivity(
                                    Intent(
                                        Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
                                        Uri.parse("package:net.appitiza.kuniyapally")
                                    )
                                )

                            }
                            .show()
                    }

                    else -> {
                        if (::mData.isInitialized) {
                            if (mData.contact.isNotEmpty()) {
                                mViewModel.loadCall(mData.contact)
                            }

                        }

                    }


                }
            }
        }


    }

    private val listenerEdit = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            when (intent?.action) {
                Constants.ACTION_BROADCAST_EDIT -> {

                    mViewModel.getEntryDetails(mServerCode)


                }

            }

        }
    }

    override fun onDestroy() {
        LocalBroadcastManager.getInstance(this)
            .unregisterReceiver(listenerEdit)
        super.onDestroy()
    }
}
