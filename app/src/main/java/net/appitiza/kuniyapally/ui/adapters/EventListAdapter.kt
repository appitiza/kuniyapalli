package net.appitiza.kuniyapally.ui.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.event_item.view.*
import net.appitiza.kuniyapally.R
import net.appitiza.kuniyapally.callbacks.EntryClickCallBack
import net.appitiza.kuniyapally.model.DisplayDataModel

class EventListAdapter(
    val context: Context,
    val displayData: ArrayList<DisplayDataModel>,
    val callback: EntryClickCallBack
) :
    RecyclerView.Adapter<EventListAdapter.EntryListViewHolder>() {

    inner class EntryListViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(mItem: DisplayDataModel, callback: EntryClickCallBack) {
            itemView.rvContent.setOnClickListener {
                callback.onEntryClick(itemView, adapterPosition)
            }

            itemView.tvCount.text = (adapterPosition + 1).toString()
            itemView.tvAlphabet.text = mItem.alphabet
            itemView.tvCode.text = mItem.code
            itemView.tvFullName.text = mItem.owner.toUpperCase()
            itemView.tvProfession.text = mItem.profession
            itemView.tvHouse.text = mItem.house


            if (mItem.area == context.getString(R.string.south)) {
                itemView.tvArea.setTextColor(
                    ContextCompat.getColor(
                        context,
                        R.color.delete_bg_color
                    )
                )
            } else {
                itemView.tvArea.setTextColor(
                    ContextCompat.getColor(
                        context,
                        R.color.button_bg_green
                    )
                )
            }
            itemView.tvArea.text = mItem.area.substring(0, 1).toUpperCase()
            if (adapterPosition == 0) {
                itemView.llLetter.background =
                    ContextCompat.getDrawable(context, R.drawable.first_letter_drawable)
            } else if (adapterPosition == 1) {
                itemView.llLetter.background =
                    ContextCompat.getDrawable(context, R.drawable.second_letter_drawable)
            } else if ((adapterPosition % 2) == 0) {
                itemView.llLetter.background =
                    ContextCompat.getDrawable(context, R.drawable.third_letter_drawable)
            } else if ((adapterPosition % 3) == 0) {
                itemView.llLetter.background =
                    ContextCompat.getDrawable(context, R.drawable.fourth_letter_drawable)
            } else {
                itemView.llLetter.background =
                    ContextCompat.getDrawable(context, R.drawable.first_letter_drawable)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): EntryListViewHolder {
        val inflater: LayoutInflater = LayoutInflater.from(parent.context)
        return EntryListViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.event_item, parent, false)
        )

    }

    override fun getItemCount(): Int {
        return displayData.size
    }

    override fun onBindViewHolder(holder: EntryListViewHolder, position: Int) {
        holder.bind(displayData[position], callback)
    }
}