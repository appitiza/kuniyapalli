package net.appitiza.kuniyapally.ui.activities

import android.os.Bundle
import androidx.lifecycle.ViewModelProvider
import net.appitiza.kuniyapally.R
import net.appitiza.kuniyapally.viewmodels.splasviewmodels.SplashViewModel
import net.appitiza.kuniyapally.viewmodels.splasviewmodels.SplashViewModelFactory

class SplashActivity : BaseActivity() {
    private lateinit var mViewModel: SplashViewModel
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        hideToolbar()
        initialize()
    }

    fun initialize() {
        mViewModel =
            ViewModelProvider(this, SplashViewModelFactory(this)).get(SplashViewModel::class.java)
        mViewModel.launchMainActivityWithDelay()

    }
}