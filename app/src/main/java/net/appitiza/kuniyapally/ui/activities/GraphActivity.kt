package net.appitiza.kuniyapally.ui.activities

import android.graphics.Color
import android.os.Bundle
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.github.mikephil.charting.animation.Easing
import com.github.mikephil.charting.data.PieData
import com.github.mikephil.charting.data.PieDataSet
import com.github.mikephil.charting.data.PieEntry
import com.github.mikephil.charting.formatter.PercentFormatter
import com.github.mikephil.charting.utils.ColorTemplate
import com.github.mikephil.charting.utils.MPPointF
import kotlinx.android.synthetic.main.activity_graph.*
import net.appitiza.kuniyapally.R
import net.appitiza.kuniyapally.utils.OperationStatus
import net.appitiza.kuniyapally.viewmodels.graph.GraphViewModel
import net.appitiza.kuniyapally.viewmodels.graph.GraphViewModelFactory

class GraphActivity : BaseActivity() {
    private lateinit var mViewModel: GraphViewModel
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_graph)
        hideToolbar()
        initialize()
        initializeGraph()
    }

    fun initialize() {

        mViewModel =
            ViewModelProvider(this, GraphViewModelFactory(this)).get(GraphViewModel::class.java)

        mViewModel.getEntryLiveData().observe(
            this,
            Observer {
                this.run {

                    setArea(
                        it.filter { it.area == getString(R.string.north) }.size,
                        it.filter { it.area == getString(R.string.south) }.size
                    )
                    setProfession(
                        it.filter { it.profession == getString(R.string.abroad) }.size,
                        it.filter { it.profession == getString(R.string.labour) }.size,
                        it.filter { it.profession == getString(R.string.doctor) }.size,
                        it.filter { it.profession == getString(R.string.engineer) }.size,
                        it.filter { it.profession == getString(R.string.business) }.size,
                        it.filter { it.profession == getString(R.string.self_employee) }.size,
                        it.filter { it.profession == getString(R.string.gov_employee) }.size
                    )
                }
            })
        mViewModel.getStatus().observe(
            this,
            Observer { status ->

                this.run {
                    when (status) {
                        is OperationStatus.Processing -> {
                            //display progress
                            showProgress()
                        }
                        is OperationStatus.Failed -> {
                            //dismiss progress
                            dismissProgress()
                        }
                        is OperationStatus.Success -> {
                            //dismiss progress
                            dismissProgress()

                        }
                        is OperationStatus.ValidationFailed -> {
                            //dismiss progress
                            dismissProgress()


                        }

                    }

                }
            })
        mViewModel.fetchData()
        //mViewModel.autoSaveReport()
    }

    private fun initializeGraph() {
        chartArea.setUsePercentValues(true)
        chartArea.description.isEnabled = false
        chartArea.setExtraOffsets(5f, 10f, 5f, 5f)
        chartArea.dragDecelerationFrictionCoef = 0.95f
        chartArea.centerText = ""
        chartArea.isDrawHoleEnabled = false
        chartArea.setHoleColor(Color.WHITE)
        chartArea.setTransparentCircleColor(Color.WHITE)
        chartArea.setTransparentCircleAlpha(110)
        chartArea.holeRadius = 58f
        chartArea.transparentCircleRadius = 61f
        chartArea.setDrawCenterText(true)
        chartArea.rotationAngle = 0f
        chartArea.isRotationEnabled = true
        chartArea.isHighlightPerTapEnabled = true
        chartArea.animateY(2000, Easing.EaseInOutQuad)
        /* val l = chartArea.legend
         l.verticalAlignment = Legend.LegendVerticalAlignment.TOP
         l.horizontalAlignment = Legend.LegendHorizontalAlignment.RIGHT
         l.orientation = Legend.LegendOrientation.VERTICAL
         l.setDrawInside(false)
         l.xEntrySpace = 7f
         l.yEntrySpace = 0f
         l.yOffset = 0f*/
        chartArea.setEntryLabelColor(Color.BLUE)
        chartArea.setEntryLabelTextSize(10f)





        chartProfession.setUsePercentValues(true)
        chartProfession.description.isEnabled = false
        chartProfession.setExtraOffsets(5f, 10f, 5f, 5f)
        chartProfession.dragDecelerationFrictionCoef = 0.95f
        chartProfession.centerText = ""
        chartProfession.isDrawHoleEnabled = true
        chartProfession.setHoleColor(Color.WHITE)
        chartProfession.setTransparentCircleColor(Color.WHITE)
        chartProfession.setTransparentCircleAlpha(110)
        chartProfession.holeRadius = 40f
        chartProfession.transparentCircleRadius = 61f
        chartProfession.setDrawCenterText(true)
        chartProfession.rotationAngle = 0f
        chartProfession.isRotationEnabled = true
        chartProfession.isHighlightPerTapEnabled = true
        chartProfession.animateY(2000, Easing.EaseInOutQuad)
        /*val lcanditateVote = chartProfession.legend
        lcanditateVote.verticalAlignment = Legend.LegendVerticalAlignment.TOP
        lcanditateVote.horizontalAlignment = Legend.LegendHorizontalAlignment.RIGHT
        lcanditateVote.orientation = Legend.LegendOrientation.VERTICAL
        lcanditateVote.setDrawInside(false)
        lcanditateVote.xEntrySpace = 7f
        lcanditateVote.yEntrySpace = 0f
        lcanditateVote.yOffset = 0f*/
        chartProfession.setEntryLabelColor(Color.BLUE)
        chartProfession.setEntryLabelTextSize(10f)

    }

    private fun setArea(north: Int, south: Int) {
        val entries: ArrayList<PieEntry> = ArrayList()

        entries.add(PieEntry(north.toFloat(), getString(R.string.north)))
        entries.add(PieEntry(south.toFloat(), getString(R.string.south)))

        val dataSet = PieDataSet(entries, "Area")
        dataSet.setDrawIcons(false)
        dataSet.sliceSpace = 3f
        dataSet.iconsOffset = MPPointF(0f, 40f)
        dataSet.selectionShift = 5f
        // add a lot of colors
        val colors: ArrayList<Int> = ArrayList()
        for (c in ColorTemplate.VORDIPLOM_COLORS) colors.add(c)
        for (c in ColorTemplate.PASTEL_COLORS) colors.add(c)

        colors.add(ColorTemplate.getHoloBlue())
        dataSet.colors = colors
        //dataSet.setSelectionShift(0f);
        val data = PieData(dataSet)
        data.setValueFormatter(PercentFormatter(chartArea))
        data.setValueTextSize(16f)
        data.setValueTextColor(Color.BLUE)
        // data.setValueTypeface(tfLight)
        chartArea.data = data
        // undo all highlights
        chartArea.highlightValues(null)
        chartArea.invalidate()
    }

    private fun setProfession(
        abroad: Int,
        kooli: Int,
        doctor: Int,
        engineer: Int,
        business: Int,
        self: Int,
        gov: Int
    ) {
        val entries: ArrayList<PieEntry> = ArrayList()

        if (abroad > 0) {
            entries.add(PieEntry(abroad.toFloat(), getString(R.string.abroad)))
        }
        if (kooli > 0) {
            entries.add(PieEntry(kooli.toFloat(), getString(R.string.labour)))
        }
        if (doctor > 0) {
            entries.add(PieEntry(doctor.toFloat(), getString(R.string.doctor)))
        }
        if (engineer > 0) {
            entries.add(PieEntry(engineer.toFloat(), getString(R.string.engineer)))
        }
        if (business > 0) {
            entries.add(PieEntry(business.toFloat(), getString(R.string.business)))
        }
        if (self > 0) {
            entries.add(PieEntry(self.toFloat(), getString(R.string.self_employee)))
        }
        if (gov > 0) {
            entries.add(PieEntry(gov.toFloat(), getString(R.string.gov_employee)))
        }

        val dataSet = PieDataSet(entries, "Profession")
        dataSet.setDrawIcons(false)
        dataSet.sliceSpace = 3f
        dataSet.iconsOffset = MPPointF(0f, 40f)
        dataSet.selectionShift = 5f
        // add a lot of colors
        val colors: ArrayList<Int> = ArrayList()
        for (c in ColorTemplate.VORDIPLOM_COLORS) colors.add(c)
        for (c in ColorTemplate.PASTEL_COLORS) colors.add(c)
        for (c in ColorTemplate.MATERIAL_COLORS) colors.add(c)
        for (c in ColorTemplate.COLORFUL_COLORS) colors.add(c)
        for (c in ColorTemplate.LIBERTY_COLORS) colors.add(c)
        for (c in ColorTemplate.VORDIPLOM_COLORS) colors.add(c)
        for (c in ColorTemplate.COLORFUL_COLORS) colors.add(c)

        colors.add(ColorTemplate.getHoloBlue())
        dataSet.colors = colors
        //dataSet.setSelectionShift(0f);
        val data = PieData(dataSet)
        data.setValueFormatter(PercentFormatter(chartProfession))
        data.setValueTextSize(16f)
        data.setValueTextColor(Color.BLUE)
        // data.setValueTypeface(tfLight)
        chartProfession.data = data
        // undo all highlights
        chartProfession.highlightValues(null)
        chartProfession.invalidate()
    }
}