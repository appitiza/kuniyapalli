package net.appitiza.kuniyapally.ui.activities

import android.content.Context
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.FrameLayout
import android.widget.ImageButton
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.coordinatorlayout.widget.CoordinatorLayout
import net.appitiza.kuniyapally.R


open class BaseActivity : AppCompatActivity() {
    private lateinit var mTextViewScreenTitle: TextView
    private lateinit var mImageButtonBack: ImageButton
    private lateinit var mImageButtonOPtion: ImageButton
    private lateinit var mImageButtonSettings: ImageButton
    private lateinit var mImageButtonEvents: ImageButton
    private lateinit var coordinatorLayout: CoordinatorLayout
    private lateinit var activityContainer: FrameLayout
    private lateinit var toolbar: Toolbar
    private lateinit var mFlProgress: FrameLayout

    override fun setContentView(layoutResID: Int) {
        val nullParent: ViewGroup? = null
        coordinatorLayout =
            layoutInflater.inflate(R.layout.activity_base, nullParent) as CoordinatorLayout
        activityContainer = coordinatorLayout.findViewById(R.id.layout_container) as FrameLayout
        mFlProgress = coordinatorLayout.findViewById(R.id.flProgress) as FrameLayout
        toolbar = coordinatorLayout.findViewById(R.id.toolbar) as Toolbar

        mTextViewScreenTitle = coordinatorLayout.findViewById(R.id.text_screen_title) as TextView
        mImageButtonBack = coordinatorLayout.findViewById(R.id.image_back_button) as ImageButton
        mImageButtonOPtion = coordinatorLayout.findViewById(R.id.image_option_button) as ImageButton
        mImageButtonSettings = coordinatorLayout.findViewById(R.id.image_settings) as ImageButton
        mImageButtonEvents = coordinatorLayout.findViewById(R.id.image_events) as ImageButton

        layoutInflater.inflate(layoutResID, activityContainer, true)

        super.setContentView(coordinatorLayout)
    }


    fun showToolbar() {
        toolbar.visibility = View.VISIBLE
    }

    fun hideToolbar() {
        toolbar.visibility = View.GONE
    }

    fun hideSettings() {
        getSettings().visibility = View.INVISIBLE
    }
    fun hideOption() {
        getOptionButton().visibility = View.GONE
    }
    fun hideEvents() {
        getEvents().visibility = View.GONE
    }
    fun hideBack() {
        getBackButton().visibility = View.GONE
    }
    fun showSettings() {
        getSettings().visibility = View.VISIBLE
    }
    fun showEvents() {
        getEvents().visibility = View.VISIBLE
    }
    fun showOption() {
        getOptionButton().visibility = View.VISIBLE
    }

    fun showBack() {
        getBackButton().visibility = View.VISIBLE
    }


    fun setScreenTitle(resId: Int) {
        mTextViewScreenTitle.text = getString(resId)
    }

    fun setScreenTitle(title: String) {
        mTextViewScreenTitle.text = title
    }

    fun getSettings(): ImageButton {
        return mImageButtonSettings
    }
    fun getBackButton(): ImageButton {
        return mImageButtonBack
    }

    fun getOptionButton(): ImageButton {
        return mImageButtonOPtion
    }
    fun getEvents(): ImageButton {
        return mImageButtonEvents
    }

    fun showProgress() {
        mFlProgress.visibility = View.VISIBLE

    }

    fun dismissProgress() {
        mFlProgress.visibility = View.GONE

    }

    fun hideKeybaord(v: View) {
        val inputMethodManager: InputMethodManager =
            getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        inputMethodManager.hideSoftInputFromWindow(v.applicationWindowToken, 0)
    }

}