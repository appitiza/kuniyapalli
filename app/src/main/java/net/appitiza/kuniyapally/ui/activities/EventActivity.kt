package net.appitiza.kuniyapally.ui.activities

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import kotlinx.android.synthetic.main.activity_event.*
import kotlinx.android.synthetic.main.activity_main.etFullName
import kotlinx.android.synthetic.main.activity_main.rvData
import kotlinx.android.synthetic.main.activity_main.srData
import kotlinx.android.synthetic.main.activity_main.tvNoData
import net.appitiza.kuniyapally.R
import net.appitiza.kuniyapally.callbacks.DialogEventStatusCallback
import net.appitiza.kuniyapally.callbacks.DialogOkCallback
import net.appitiza.kuniyapally.callbacks.EntryClickCallBack
import net.appitiza.kuniyapally.constants.Constants
import net.appitiza.kuniyapally.model.DisplayDataModel
import net.appitiza.kuniyapally.model.EventEntityDataModel
import net.appitiza.kuniyapally.ui.adapters.EntryListAdapter
import net.appitiza.kuniyapally.ui.adapters.EventListAdapter
import net.appitiza.kuniyapally.utils.DialogUtils
import net.appitiza.kuniyapally.utils.OperationStatus
import net.appitiza.kuniyapally.utils.PreferenceHelper
import net.appitiza.kuniyapally.viewmodels.events.EventsViewModel
import net.appitiza.kuniyapally.viewmodels.events.EventsViewModelFactory


class EventActivity : BaseActivity() {
    private lateinit var mViewModel: EventsViewModel
    private lateinit var mAdapter: EventListAdapter
    private var mDataList = ArrayList<DisplayDataModel>()
    private lateinit var mUserEvent: EventEntityDataModel

    private var mLoggedInAsAdmin by PreferenceHelper(Constants.PREF_LOGGEDIN_AS_AdMIN, false)
    private var mLastUpdatedTime by PreferenceHelper(Constants.PREF_LAST_UPDATED_TIME, "")

    var selectedUserID: Int = 0
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_event)
        showToolbar()
        showBack()
        hideSettings()
        hideOption()
        hideEvents()


        initialize()
        setClick()
    }

    override fun onStart() {
        LocalBroadcastManager.getInstance(this)
            .registerReceiver(
                listenerEdit,
                IntentFilter(Constants.ACTION_BROADCAST_EDIT)
            )
        super.onStart()

    }

    fun initialize() {
        mViewModel =
            ViewModelProvider(this, EventsViewModelFactory(this)).get(EventsViewModel::class.java)

        mAdapter = EventListAdapter(this, mDataList, object : EntryClickCallBack {
            override fun onEntryClick(view: View, entryId: Int) {
                selectedUserID = entryId
                mViewModel.getEventStatus(entryId)
            }

            override fun onDeleteClick(view: View, position: Int) {

            }
        })
        rvData.layoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)
        rvData.adapter = mAdapter
        mViewModel =
            ViewModelProvider(this, EventsViewModelFactory(this)).get(EventsViewModel::class.java)

        mViewModel.getEntryLiveData().observe(
            this,
            Observer {
                this.run {

                    mDataList.clear()
                    mDataList.addAll(it)
                    mAdapter.notifyDataSetChanged()
                    if (mDataList.isEmpty()) {
                        tvNoData.visibility = View.VISIBLE
                    } else {
                        tvNoData.visibility = View.GONE
                    }
                }
            })

        mViewModel.getEventLiveData().observe(
            this,
            Observer {
                this.run {

                    val displayText = it?.entryData ?: getString(R.string.uncollected)
                    DialogUtils(this@EventActivity).showEventStatusDialog(displayText,
                        object : DialogEventStatusCallback {
                            override fun onCollectedClick() {
                                val selectedEventId = it?.eventId ?: 0
                                mViewModel.changeStatus(
                                    getString(R.string.collected),
                                    selectedUserID,
                                    selectedEventId
                                )
                            }

                            override fun onUncollectedClick() {
                                mViewModel.changeStatus(
                                    getString(R.string.uncollected),
                                    selectedUserID,
                                    it.eventId
                                )
                            }

                        })
                }
            })

        mViewModel.getStatus().observe(
            this,
            Observer { status ->

                this.run {
                    when (status) {
                        is OperationStatus.Processing -> {
                            //display progress
                            showProgress()
                        }
                        is OperationStatus.Failed -> {
                            //dismiss progress
                            dismissProgress()
                        }
                        is OperationStatus.Success -> {
                            //dismiss progress
                            dismissProgress()


                        }
                        is OperationStatus.ValidationFailed -> {
                            //dismiss progress
                            dismissProgress()
                            DialogUtils(this@EventActivity).showDialog(
                                getString(R.string.sure_to_delete)
                            )

                        }

                    }

                }
            })
        mViewModel.fetchData()
        //mViewModel.autoSaveReport()
    }

    private fun setClick() {

        getBackButton().setOnClickListener { finish() }

        btnReset.setOnClickListener {
            DialogUtils(this).showOkDialog(getString(R.string.are_sure_reset),
                object : DialogOkCallback {
                    override fun okClick() {
                        mViewModel.reset()
                    }

                })
        }

        srData.setOnRefreshListener(SwipeRefreshLayout.OnRefreshListener {
            srData.isRefreshing = false
            if (etFullName.text.trim().isNotEmpty()) {
                mViewModel.searchData(
                    etFullName.text.toString()
                )
            } else {
                mViewModel.fetchData()
            }
        })
        etFullName.addTextChangedListener(object : TextWatcher {

            override fun afterTextChanged(s: Editable) {
                mViewModel.searchData(etFullName.text.toString())
            }

            override fun beforeTextChanged(
                s: CharSequence, start: Int,
                count: Int, after: Int
            ) {
            }

            override fun onTextChanged(
                s: CharSequence, start: Int,
                before: Int, count: Int
            ) {
            }
        })

    }


    private val listenerEdit = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            when (intent?.action) {
                Constants.ACTION_BROADCAST_EDIT -> {

                    if (etFullName.text.trim().isNotEmpty()) {
                        mViewModel.searchData(
                            etFullName.text.toString()
                        )
                    } else {
                        mViewModel.fetchData()
                    }


                }

            }

        }
    }

    override fun onDestroy() {
        LocalBroadcastManager.getInstance(this)
            .unregisterReceiver(listenerEdit)
        super.onDestroy()
    }
}