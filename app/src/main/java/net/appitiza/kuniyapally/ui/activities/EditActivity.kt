package net.appitiza.kuniyapally.ui.activities

import android.content.Intent
import android.os.Bundle
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import kotlinx.android.synthetic.main.activity_edit.*
import kotlinx.android.synthetic.main.activity_edit.etAddress
import kotlinx.android.synthetic.main.activity_edit.etArea
import kotlinx.android.synthetic.main.activity_edit.etContact
import kotlinx.android.synthetic.main.activity_edit.etFullName
import kotlinx.android.synthetic.main.activity_edit.etHouseName
import kotlinx.android.synthetic.main.activity_edit.etProfession
import kotlinx.android.synthetic.main.activity_edit.etWhatsAppNumber
import net.appitiza.kuniyapally.R
import net.appitiza.kuniyapally.callbacks.DialogItemClickCallback
import net.appitiza.kuniyapally.callbacks.DialogOkCallback
import net.appitiza.kuniyapally.constants.Constants
import net.appitiza.kuniyapally.model.EntryDataModel
import net.appitiza.kuniyapally.utils.DialogUtils
import net.appitiza.kuniyapally.utils.OperationStatus
import net.appitiza.kuniyapally.utils.TaskEnum
import net.appitiza.kuniyapally.viewmodels.edit.EditViewModel
import net.appitiza.kuniyapally.viewmodels.edit.EditViewModelFactory

class EditActivity : BaseActivity() {
    private lateinit var mViewModel: EditViewModel
    lateinit var mEntryData : EntryDataModel
    private var mServercode: String = ""
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_edit)
        showToolbar()
        showBack()
        hideSettings()
        hideEvents()
        hideOption()
        initialize()
        setClick()
    }

    private fun initialize() {
        mServercode = intent.getStringExtra("servercode").toString()
        mViewModel =
            ViewModelProvider(this, EditViewModelFactory(this)).get(EditViewModel::class.java)
        mViewModel.getEntryLiveData().observe(
            this,
            Observer<EntryDataModel> { entrymodel ->
                this.run {

                    mEntryData = entrymodel
                    setDetails(mEntryData)

                }
            })
        mViewModel.getStatus().observe(this, androidx.lifecycle.Observer { status ->
            when (status) {
                is OperationStatus.Processing -> {
                    //display progress
                    showProgress()
                }
                is OperationStatus.Failed -> {
                    //dismiss progress
                    dismissProgress()

                }
                is OperationStatus.Success -> {
                    //dismiss progress
                    dismissProgress()

                    if (status.task == TaskEnum.EDIT_DATA) {
                        val localBroadcastManager: LocalBroadcastManager =
                            LocalBroadcastManager.getInstance(this)
                        val localIntent = Intent(Constants.ACTION_BROADCAST_EDIT)
                        localBroadcastManager.sendBroadcast(localIntent)

                        DialogUtils(this).showOkDialog(status.message,
                            object : DialogOkCallback {
                                override fun okClick() {
                                    finish()
                                }
                            })
                    }
                }
                is OperationStatus.ValidationFailed -> {
                    //dismiss progress
                    dismissProgress()

                }

            }

        })
        mViewModel.getEntryDetails(mServercode)

    }

    private fun setClick() {

        getBackButton().setOnClickListener { finish() }

        etProfession.setOnClickListener {
            DialogUtils(this).showProfessionDialog(object : DialogItemClickCallback {
                override fun itemClick(text: String) {
                    etProfession.setText(text)
                }
            })
        }
        etArea.setOnClickListener {
            DialogUtils(this).showAreaDialog(object : DialogItemClickCallback {
                override fun itemClick(text: String) {
                    etArea.setText(text)
                }
            })
        }

        btnUpdate.setOnClickListener {
            mViewModel.editData(
                mEntryData.entryid,
                mServercode,
                etCode.text.toString(),
                etFullName.text.toString(),
                etProfession.text.toString(),
                etArea.text.toString(),
                etHouseName.text.toString(),
                etAddress.text.toString(),
                etContact.text.toString(),
                etWhatsAppNumber.text.toString()
            )
        }
    }

    private fun setDetails(entry: EntryDataModel) {
        etCode.setText(entry.code)
        etFullName.setText(entry.owner)
        etProfession.setText(entry.profession)
        etArea.setText(entry.area)
        etHouseName.setText(entry.housename)
        etAddress.setText(entry.address)
        etContact.setText(entry.contact)
        etWhatsAppNumber.setText(entry.whatsapp)

    }


}
