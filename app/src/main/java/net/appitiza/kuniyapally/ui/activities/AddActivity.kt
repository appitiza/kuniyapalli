package net.appitiza.kuniyapally.ui.activities

import android.content.Intent
import android.os.Bundle
import androidx.lifecycle.ViewModelProvider
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import kotlinx.android.synthetic.main.activity_add.*
import net.appitiza.kuniyapally.R
import net.appitiza.kuniyapally.callbacks.DialogItemClickCallback
import net.appitiza.kuniyapally.callbacks.DialogOkCallback
import net.appitiza.kuniyapally.constants.Constants
import net.appitiza.kuniyapally.utils.DialogUtils
import net.appitiza.kuniyapally.utils.OperationStatus
import net.appitiza.kuniyapally.viewmodels.add.AddViewModel
import net.appitiza.kuniyapally.viewmodels.add.AddViewModelFactory


class AddActivity : BaseActivity() {

    private lateinit var mViewModel: AddViewModel
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add)
        showToolbar()
        showBack()
        hideSettings()
        hideEvents()
        hideOption()
        initialize()
        setClick()


    }


    private fun initialize() {


        mViewModel =
            ViewModelProvider(this, AddViewModelFactory(this)).get(AddViewModel::class.java)

        mViewModel.getStatus().observe(this, androidx.lifecycle.Observer { status ->
            when (status) {
                is OperationStatus.Processing -> {
                    //display progress
                    showProgress()
                }
                is OperationStatus.Failed -> {
                    //dismiss progress
                    dismissProgress()

                }
                is OperationStatus.Success -> {
                    //dismiss progress
                    dismissProgress()

                    val localBroadcastManager: LocalBroadcastManager =
                        LocalBroadcastManager.getInstance(this)
                    val localIntent = Intent(Constants.ACTION_BROADCAST_EDIT)
                    localBroadcastManager.sendBroadcast(localIntent)

                    DialogUtils(this).showOkDialog(status.message,
                        object : DialogOkCallback {
                            override fun okClick() {

                                clearEdittext()
                            }
                        })
                }
                is OperationStatus.ValidationFailed -> {
                    //dismiss progress
                    dismissProgress()
                    DialogUtils(this).showDialog(status.message)
                }

            }

        })


    }

    private fun clearEdittext() {
        etCode.text.clear()
        etFullName.text.clear()
        etHouseName.text.clear()
        etProfession.text.clear()
        etArea.text.clear()
        etAddress.text.clear()
        etContact.text.clear()
        etWhatsAppNumber.text.clear()
        etCode.requestFocus()
    }

    private fun setClick() {

        getBackButton().setOnClickListener { finish() }
        btnAdd.setOnClickListener {
            mViewModel.addData(
                etCode.text.toString(),
                etFullName.text.toString(),
                etProfession.text.toString(),
                etArea.text.toString(),
                etHouseName.text.toString(),
                etAddress.text.toString(),
                etContact.text.toString(),
                etWhatsAppNumber.text.toString()
            )
        }

        etProfession.setOnClickListener {
            DialogUtils(this).showProfessionDialog(object : DialogItemClickCallback {
                override fun itemClick(text: String) {
                    etProfession.setText(text)
                }
            })
        }
        etArea.setOnClickListener {
            DialogUtils(this).showAreaDialog(object : DialogItemClickCallback {
                override fun itemClick(text: String) {
                    etArea.setText(text)
                }
            })
        }
    }
}
