package net.appitiza.kuniyapally.ui.activities

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.provider.Settings
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.activity_main.*
import net.appitiza.kuniyapally.R
import net.appitiza.kuniyapally.callbacks.DialogOkCallback
import net.appitiza.kuniyapally.callbacks.DialogSettingsCallback
import net.appitiza.kuniyapally.callbacks.EntryClickCallBack
import net.appitiza.kuniyapally.constants.Constants
import net.appitiza.kuniyapally.model.DisplayDataModel
import net.appitiza.kuniyapally.ui.adapters.EntryListAdapter
import net.appitiza.kuniyapally.utils.DialogUtils
import net.appitiza.kuniyapally.utils.OperationStatus
import net.appitiza.kuniyapally.utils.PreferenceHelper
import net.appitiza.kuniyapally.utils.TaskEnum
import net.appitiza.kuniyapally.viewmodels.main.MainViewModel
import net.appitiza.kuniyapally.viewmodels.main.MainViewModelFactory


class MainActivity : BaseActivity() {
    private lateinit var mViewModel: MainViewModel
    private lateinit var mAdapter: EntryListAdapter
    private var mDataList = ArrayList<DisplayDataModel>()
    private var mLastUpdatedTime by PreferenceHelper(Constants.PREF_LAST_UPDATED_TIME, "0")
    private var mLoggedInAsAdmin by PreferenceHelper(Constants.PREF_LOGGEDIN_AS_AdMIN, false)
    private var mLoggedInAsGuest by PreferenceHelper(Constants.PREF_LOGGEDIN_AS_GUEST, false)
    private var mLoggedInUserEmail by PreferenceHelper(Constants.PREF_LOGGEDIN_USER_EMAIL, "")
    private var mLoggedInUserPassword by PreferenceHelper(Constants.PREF_LOGGEDIN_USER_PASSWORD, "")
    private var mLoggedIn by PreferenceHelper(Constants.PREF_LOGGEDIN, false)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        showToolbar()
        hideBack()
        if (mLoggedInAsAdmin) {
            showSettings()
            showOption()
            showEvents()
            fabAdd.visibility = View.VISIBLE
        } else {
            hideSettings()
            hideOption()
            hideEvents()
            fabAdd.visibility = View.GONE
        }

        initialize()
        setClick()
    }

    override fun onStart() {
        LocalBroadcastManager.getInstance(this)
            .registerReceiver(
                listenerEdit,
                IntentFilter(Constants.ACTION_BROADCAST_EDIT)
            )
        super.onStart()

    }

    fun initialize() {
        mViewModel =
            ViewModelProvider(this, MainViewModelFactory(this)).get(MainViewModel::class.java)

        mAdapter = EntryListAdapter(this, mDataList, object : EntryClickCallBack {
            override fun onEntryClick(view: View, position: Int) {
                mViewModel.moveToDetails(view, mDataList[position].servercode)
            }

            override fun onDeleteClick(view: View, position: Int) {
                if (mLoggedIn && mLoggedInAsAdmin) {
                    DialogUtils(this@MainActivity).showOkDialog(
                        getString(R.string.sure_to_delete),
                        object : DialogOkCallback {
                            override fun okClick() {
                                mViewModel.delete(mDataList[position].servercode)
                            }

                        })
                } else {
                    DialogUtils(this@MainActivity).showDialog(
                        getString(R.string.sorr_you_dont_have_permission)
                    )
                }


            }
        })
        rvData.layoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)
        rvData.adapter = mAdapter

        mViewModel.getEntryLiveData().observe(
            this,
            Observer {
                this.run {

                    mDataList.clear()
                    mDataList.addAll(it)
                    mAdapter.notifyDataSetChanged()
                    if (mDataList.isEmpty()) {
                        tvNoData.visibility = View.VISIBLE
                    } else {
                        tvNoData.visibility = View.GONE
                    }
                }
            })
        mViewModel.getStatus().observe(
            this,
            Observer { status ->

                this.run {
                    when (status) {
                        is OperationStatus.Processing -> {
                            //display progress
                            showProgress()
                        }
                        is OperationStatus.Failed -> {
                            //dismiss progress
                            dismissProgress()
                            when (status.task) {

                                TaskEnum.RESET_PASSWORD -> {
                                    DialogUtils(this@MainActivity).showDialog(
                                        getString(
                                            R.string.sorry_failed_reset_password
                                        )
                                    )
                                }
                                TaskEnum.LOGOUT -> {
                                    DialogUtils(this@MainActivity).showDialog(
                                        getString(
                                            R.string.failed_to_logout
                                        )
                                    )
                                }
                                else -> {

                                }
                            }
                        }
                        is OperationStatus.Success -> {
                            //dismiss progress
                            dismissProgress()
                            when (status.task) {
                                TaskEnum.RESET_PASSWORD -> {
                                    DialogUtils(this@MainActivity).showDialog(
                                        getString(
                                            R.string.reset_password_link_sent
                                        )
                                    )
                                }
                                TaskEnum.PDF_GENERATION -> {
                                    DialogUtils(this@MainActivity).showOkDialog(
                                        getString(R.string.would_you_like_to_open),
                                        object : DialogOkCallback {
                                            override fun okClick() {
                                                mViewModel.openPDF()
                                            }
                                        }

                                    )

                                }
                                TaskEnum.LOGOUT -> {
                                    mViewModel.moveToLogin()
                                }
                            }


                        }
                        is OperationStatus.ValidationFailed -> {
                            //dismiss progress
                            dismissProgress()
                            DialogUtils(this@MainActivity).showDialog(
                                getString(R.string.sure_to_delete)
                            )

                        }

                    }

                }
            })
        mViewModel.fetchData()
        //mViewModel.autoSaveReport()
    }

    private fun setClick() {

        fabAdd.setOnClickListener {
            if (mLoggedInAsAdmin) {
                mViewModel.moveToAdd()
            }

        }

        getEvents().setOnClickListener {
            if (mLoggedInAsAdmin) {
                mViewModel.moveToEvent()
            }

        }
        getSettings().setOnClickListener {
            DialogUtils(this).showSettingDialog(object : DialogSettingsCallback {


                override fun graphClick() {
                    mViewModel.moveToGraph()
                }

                override fun changePasswordClick() {
                    mViewModel.resetPassword()
                }

                override fun logout() {
                    mViewModel.logout()
                }

            })
        }
        getOptionButton().setOnClickListener {
            if (mLoggedInAsAdmin) {
                mViewModel.generatePdf(etFullName.text.toString())
            }

        }
        etFullName.addTextChangedListener(object : TextWatcher {

            override fun afterTextChanged(s: Editable) {
                mViewModel.searchData(etFullName.text.toString())
            }

            override fun beforeTextChanged(
                s: CharSequence, start: Int,
                count: Int, after: Int
            ) {
            }

            override fun onTextChanged(
                s: CharSequence, start: Int,
                before: Int, count: Int
            ) {
            }
        })

        srData.setOnRefreshListener(SwipeRefreshLayout.OnRefreshListener {
            srData.isRefreshing = false
            if (etFullName.text.trim().isNotEmpty()) {
                mViewModel.searchData(
                    etFullName.text.toString()
                )
            } else {
                mViewModel.fetchData()
            }
        })


    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        when (requestCode) {
            Constants.REQUEST_PERMISSIONSREQUEST_STORAGE_CODE -> {
                when {

                    grantResults.any { it != PackageManager.PERMISSION_GRANTED } -> {
                        Snackbar
                            .make(
                                etFullName,
                                getString(R.string.need_storage_permission),
                                Snackbar.LENGTH_LONG
                            )
                            .setAction(R.string.ok) {

                                startActivity(
                                    Intent(
                                        Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
                                        Uri.parse("package:net.appitiza.kuniyapally")
                                    )
                                )

                            }
                            .show()
                    }

                    else -> {
                        mViewModel.generatePdf(etFullName.text.toString())

                    }


                }
            }
        }
    }

    private val listenerEdit = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            when (intent?.action) {
                Constants.ACTION_BROADCAST_EDIT -> {

                    if (etFullName.text.trim().isNotEmpty()) {
                        mViewModel.searchData(
                            etFullName.text.toString()
                        )
                    } else {
                        mViewModel.fetchData()
                    }


                }

            }

        }
    }

    override fun onDestroy() {
        LocalBroadcastManager.getInstance(this)
            .unregisterReceiver(listenerEdit)
        super.onDestroy()
    }
}