package net.appitiza.kuniyapally.ui.activities

import android.os.Bundle
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import kotlinx.android.synthetic.main.activity_login_as_guest.*
import net.appitiza.kuniyapally.R
import net.appitiza.kuniyapally.utils.DialogUtils
import net.appitiza.kuniyapally.utils.OperationStatus
import net.appitiza.kuniyapally.utils.TaskEnum
import net.appitiza.kuniyapally.viewmodels.guestviewmodels.GuestViewModel
import net.appitiza.kuniyapally.viewmodels.guestviewmodels.GuestViewModelFactory

class LoginAsGuestActivity : BaseActivity() {
    private lateinit var mViewModel: GuestViewModel
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login_as_guest)
        hideToolbar()
        initialize()
        setClick()
    }

    private fun setClick() {
        btnNext.setOnClickListener { mViewModel.login( etPhone.text.toString().trim()) }


    }

    fun initialize() {
        mViewModel =
            ViewModelProvider(this, GuestViewModelFactory(this)).get(GuestViewModel::class.java)

        mViewModel.getStatus().observe(
            this,
            Observer { status ->

                this.run {
                    when (status) {
                        is OperationStatus.Processing -> {
                            //display progress
                            showProgress()
                        }
                        is OperationStatus.Failed -> {
                            //dismiss progress
                            dismissProgress()

                            when (status.task) {
                                TaskEnum.GUEST_LOGIN -> {
                                    DialogUtils(this@LoginAsGuestActivity).showDialog(
                                        getString(R.string.login_failed)
                                    )
                                }

                                else -> {

                                }
                            }
                        }
                        is OperationStatus.Success -> {
                            //dismiss progress
                            dismissProgress()
                            when (status.task) {
                                TaskEnum.GUEST_LOGIN -> {
                                    mViewModel.continueAsGuest()
                                }

                                else -> {

                                }
                            }


                        }
                        is OperationStatus.ValidationFailed -> {
                            //dismiss progress
                            dismissProgress()
                            when(status.task)
                            {
                                TaskEnum.GUEST_LOGIN -> {
                                    DialogUtils(this@LoginAsGuestActivity).showDialog(
                                        getString(R.string.phone_number_missing)
                                    )
                                }

                                else -> {

                                }
                            }


                        }

                    }

                }
            })
    }
}