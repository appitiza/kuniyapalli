package net.appitiza.kuniyapally.ui.activities

import android.os.Bundle
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import kotlinx.android.synthetic.main.activity_login.*
import net.appitiza.kuniyapally.R
import net.appitiza.kuniyapally.utils.DialogUtils
import net.appitiza.kuniyapally.utils.OperationStatus
import net.appitiza.kuniyapally.utils.TaskEnum
import net.appitiza.kuniyapally.viewmodels.loginviewmodels.LoginViewModel
import net.appitiza.kuniyapally.viewmodels.loginviewmodels.LoginViewModelFactory

class LoginActivity : BaseActivity() {
    private lateinit var mViewModel: LoginViewModel
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        hideToolbar()
        initialize()
        setClick()
    }

    private fun setClick() {
        tvContinueAsGuest.setOnClickListener { mViewModel.continueAsGuest() }
        btnNext.setOnClickListener {
            mViewModel.login(
                etEmail.text.toString().trim(),
                etPassword.text.toString().trim()
            )

        }
        tvForgotPassword.setOnClickListener {
            mViewModel.resetPassword(
                etEmail.text.toString().trim()
            )
        }

    }

    fun initialize() {
        mViewModel =
            ViewModelProvider(this, LoginViewModelFactory(this)).get(LoginViewModel::class.java)

        mViewModel.getStatus().observe(
            this,
            Observer { status ->

                this.run {
                    when (status) {
                        is OperationStatus.Processing -> {
                            //display progress
                            showProgress()
                        }
                        is OperationStatus.Failed -> {
                            //dismiss progress
                            dismissProgress()

                            when (status.task) {
                                TaskEnum.EMAIL_LOGIN -> {
                                    DialogUtils(this@LoginActivity).showDialog(
                                        getString(R.string.login_failed)
                                    )
                                }
                                TaskEnum.RESET_PASSWORD -> {
                                    DialogUtils(this@LoginActivity).showDialog(
                                        getString(
                                            R.string.sorry_failed_reset_password
                                        )
                                    )
                                }

                                else -> {

                                }
                            }
                        }
                        is OperationStatus.Success -> {
                            //dismiss progress
                            dismissProgress()
                            when (status.task) {
                                TaskEnum.EMAIL_LOGIN -> {
                                    mViewModel.launchMainActivity()
                                }
                                TaskEnum.RESET_PASSWORD -> {
                                    DialogUtils(this@LoginActivity).showDialog(
                                        getString(
                                            R.string.reset_password_link_sent
                                        )
                                    )
                                }

                                else -> {

                                }
                            }


                        }
                        is OperationStatus.ValidationFailed -> {
                            //dismiss progress
                            dismissProgress()
                            when(status.task)
                            {
                                TaskEnum.EMAIL_LOGIN -> {
                                    DialogUtils(this@LoginActivity).showDialog(
                                        getString(R.string.email_password_missing)
                                    )
                                }
                                TaskEnum.RESET_PASSWORD -> {
                                    DialogUtils(this@LoginActivity).showDialog(
                                        getString(R.string.email_missing)
                                    )
                                }
                                else -> {

                                }
                            }


                        }

                    }

                }
            })
    }
}