package net.appitiza.kuniyapally.app

import android.app.Application
import androidx.room.Room
import net.appitiza.kuniyapally.constants.Constants
import net.appitiza.kuniyapally.utils.PreferenceHelper

class BaseApp : Application() {
    var myDatabase: KuniyapallyDatabase? = null
    override fun onCreate() {
        super.onCreate()
        PreferenceHelper.init(this, Constants.PREF_NAME)
    }

    fun databaseInstance(): KuniyapallyDatabase? {
        if (myDatabase == null) {
            myDatabase = Room.databaseBuilder<KuniyapallyDatabase>(
                applicationContext,
                KuniyapallyDatabase::class.java,
                "KuniyapallyDB"
            )
                /* .addMigrations(EntryDatabase.MIGRATION_1_2)*/
                .build()
        }
        return myDatabase
    }
}