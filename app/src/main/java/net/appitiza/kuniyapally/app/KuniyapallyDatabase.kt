package net.appitiza.kuniyapally.app

import androidx.room.Database
import androidx.room.RoomDatabase
import net.appitiza.kuniyapally.model.EntryDAO
import net.appitiza.kuniyapally.model.EntryEntityDataModel
import net.appitiza.kuniyapally.model.EventEntityDataModel

@Database(entities = [(EntryEntityDataModel::class),(EventEntityDataModel::class)], version = 1, exportSchema = false)
abstract class KuniyapallyDatabase : RoomDatabase() {
    public abstract fun entryDao(): EntryDAO
    /*companion object {
        val MIGRATION_1_2: Migration = object : Migration(1, 2) {
            override fun migrate(database: SupportSQLiteDatabase) { // Since we didn't alter the table, there's nothing else to do here.
            }
        }
    }*/
}