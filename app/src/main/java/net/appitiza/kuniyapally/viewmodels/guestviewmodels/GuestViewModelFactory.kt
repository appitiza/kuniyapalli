package net.appitiza.kuniyapally.viewmodels.guestviewmodels

import android.content.Context
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import net.appitiza.kuniyapally.repositories.DataRepository
import net.appitiza.kuniyapally.repositories.LocalRepoService
import net.appitiza.kuniyapally.repositories.RemoteRepoService
import net.appitiza.kuniyapally.utils.NetworkUtil

class GuestViewModelFactory(private val context: Context) : ViewModelProvider.Factory {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return GuestViewModel(
            context = context,
            repository = getRepository()
        ) as T
    }

    private fun getRepository(): DataRepository {
        return if (!NetworkUtil(context).isNetworkAvailable()) {
            LocalRepoService(context)
        } else {
            RemoteRepoService(context)
        }
    }
}