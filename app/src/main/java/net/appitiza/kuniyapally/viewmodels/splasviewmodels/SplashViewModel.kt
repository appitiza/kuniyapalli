package net.appitiza.kuniyapally.viewmodels.splasviewmodels

import android.content.Context
import android.content.Intent
import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import kotlinx.coroutines.*
import kotlinx.coroutines.Dispatchers.Main
import net.appitiza.kuniyapally.constants.Constants
import net.appitiza.kuniyapally.model.DisplayDataModel
import net.appitiza.kuniyapally.repositories.DataRepository
import net.appitiza.kuniyapally.ui.activities.LoginActivity
import net.appitiza.kuniyapally.ui.activities.MainActivity
import net.appitiza.kuniyapally.ui.activities.SplashActivity
import net.appitiza.kuniyapally.utils.NetworkUtil
import net.appitiza.kuniyapally.utils.OperationStatus
import net.appitiza.kuniyapally.utils.PreferenceHelper
import net.appitiza.kuniyapally.utils.TaskEnum

private const val SPLASH_TIMMER = 3000L

private var mMutableLiveDataDisplayDataModel = MediatorLiveData<List<DisplayDataModel>>()
private var mOperationStatus = MutableLiveData<OperationStatus>()

private var mLoggedInAsAdmin by PreferenceHelper(Constants.PREF_LOGGEDIN_AS_AdMIN, false)
private var mLoggedInUserEmail by PreferenceHelper(Constants.PREF_LOGGEDIN_USER_EMAIL, "")
private var mLoggedInUserPassword by PreferenceHelper(Constants.PREF_LOGGEDIN_USER_PASSWORD, "")
private var mLoggedIn by PreferenceHelper(Constants.PREF_LOGGEDIN, false)

class SplashViewModel(private val context: Context, private val repository: DataRepository) :
    ViewModel() {
    private var mOperationStatus = MutableLiveData<OperationStatus>()


    fun launchMainActivityWithDelay() {

        if (mLoggedIn && mLoggedInAsAdmin) {
            if (mLoggedInUserEmail.isNotEmpty() && mLoggedInUserPassword.isNotEmpty()) {
                login(mLoggedInUserEmail, mLoggedInUserPassword)
            }
        } else {
            CoroutineScope(Main).launch { launchLoginAfter() }
        }

    }

    fun login(email: String, password: String) {
        if (NetworkUtil(context).isNetworkAvailable()) {
            mOperationStatus.value =
                OperationStatus.Processing("processing", TaskEnum.EMAIL_LOGIN)

            val job = CoroutineScope(Dispatchers.IO).launch {

                if (repository.emailLogin(email, password)) {
                    withContext(Main)
                    {
                        context.startActivity(Intent(context, MainActivity::class.java))
                        (context as SplashActivity).finish()
                    }
                } else {
                    withContext(Main)
                    {
                        context.startActivity(Intent(context, LoginActivity::class.java))
                        (context as SplashActivity).finish()
                    }
                }

            }
        } else {
            CoroutineScope(Main).launch { launchLoginAfter() }
        }
    }

    private suspend fun launchLoginAfter() {
        delay(SPLASH_TIMMER)
        context.startActivity(Intent(context, LoginActivity::class.java))
        (context as SplashActivity).finish()
    }


    fun getEntryLiveData(): LiveData<List<DisplayDataModel>> = mMutableLiveDataDisplayDataModel
    fun getStatus(): LiveData<OperationStatus> = mOperationStatus
}