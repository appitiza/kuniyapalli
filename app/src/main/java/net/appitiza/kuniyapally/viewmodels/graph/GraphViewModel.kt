package net.appitiza.kuniyapally.viewmodels.graph

import android.content.Context
import android.content.Intent
import android.graphics.*
import android.graphics.pdf.PdfDocument
import android.os.Environment
import android.util.Log
import android.view.View
import androidx.core.content.FileProvider
import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import net.appitiza.kuniyapally.R
import net.appitiza.kuniyapally.model.DisplayDataModel
import net.appitiza.kuniyapally.model.EntryEntityDataModel
import net.appitiza.kuniyapally.repositories.DataRepository
import net.appitiza.kuniyapally.ui.activities.AddActivity
import net.appitiza.kuniyapally.ui.activities.DetailedActivity
import net.appitiza.kuniyapally.ui.activities.MainActivity
import net.appitiza.kuniyapally.utils.NetworkUtil
import net.appitiza.kuniyapally.utils.OperationStatus
import net.appitiza.kuniyapally.utils.PermissionUtils
import net.appitiza.kuniyapally.utils.TaskEnum
import java.io.File
import java.io.FileOutputStream
import java.io.IOException


class GraphViewModel(private val context: Context, private val repository: DataRepository) :
    ViewModel() {
    private var mMutableLiveDataDisplayDataModel = MediatorLiveData<List<DisplayDataModel>>()
    private var mOperationStatus = MutableLiveData<OperationStatus>()



    fun fetchData() {
        mOperationStatus.value = OperationStatus.Processing("processing", TaskEnum.FETCH_DATA)
        val job = CoroutineScope(Dispatchers.IO).launch {

            getDatalistFromRepo()

        }
    }
    suspend fun getDatalistFromRepo() {
        val response = repository.getRemoteDataList()
        withContext(Dispatchers.Main)
        {
            val entry = response.map { it.toEntry() }
            val display = entry.map { it.toDisplayEntry() }
            mMutableLiveDataDisplayDataModel.value = display
            withContext(Dispatchers.Main)
            {
                mOperationStatus.value = OperationStatus.Success("success", TaskEnum.FETCH_DATA)
            }
        }

    }
    fun getEntryLiveData(): LiveData<List<DisplayDataModel>> = mMutableLiveDataDisplayDataModel
    fun getStatus(): LiveData<OperationStatus> = mOperationStatus


}