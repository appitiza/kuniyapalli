package net.appitiza.kuniyapally.viewmodels.loginviewmodels

import android.content.Context
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import net.appitiza.kuniyapally.repositories.DataRepository
import net.appitiza.kuniyapally.repositories.LocalRepoService
import net.appitiza.kuniyapally.repositories.RemoteRepoService
import net.appitiza.kuniyapally.utils.NetworkUtil

class LoginViewModelFactory(private val context: Context) : ViewModelProvider.Factory {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return LoginViewModel(
            context = context,
            repository = getRepository()
        ) as T
    }

    private fun getRepository(): DataRepository {
        return if (!NetworkUtil(context).isNetworkAvailable()) {
            LocalRepoService(context)
        } else {
            RemoteRepoService(context)
        }
    }
}