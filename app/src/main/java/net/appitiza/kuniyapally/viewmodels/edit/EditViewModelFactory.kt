package net.appitiza.kuniyapally.viewmodels.edit

import android.content.Context
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import net.appitiza.kuniyapally.repositories.DataRepository
import net.appitiza.kuniyapally.repositories.LocalRepoService
import net.appitiza.kuniyapally.repositories.RemoteRepoService
import net.appitiza.kuniyapally.utils.NetworkUtil

class EditViewModelFactory(val context: Context) : ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return EditViewModel(
            context,
            getRepository()
        ) as T
    }

    private fun getRepository(): DataRepository {
        return if (!NetworkUtil(context).isNetworkAvailable()) {
            LocalRepoService(context)
        } else {
            RemoteRepoService(context)
        }
    }
}