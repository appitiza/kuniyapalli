package net.appitiza.kuniyapally.viewmodels.add

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import net.appitiza.kuniyapally.R
import net.appitiza.kuniyapally.constants.Constants
import net.appitiza.kuniyapally.model.EntryEntityDataModel
import net.appitiza.kuniyapally.repositories.DataRepository
import net.appitiza.kuniyapally.utils.NetworkUtil
import net.appitiza.kuniyapally.utils.OperationStatus
import net.appitiza.kuniyapally.utils.PreferenceHelper
import net.appitiza.kuniyapally.utils.TaskEnum

class AddViewModel(val context: Context, private val repository: DataRepository) :
    ViewModel() {
    private var mLoggedInUserEmail by PreferenceHelper(Constants.PREF_LOGGEDIN_USER_EMAIL, "")
    private var mOperationStatus = MutableLiveData<OperationStatus>()
    fun addData(
        code: String,
        owner: String,
        profession: String,
        area: String,
        housename: String,
        address: String,
        contact: String,
        whatsapp: String
    ) {
        if(NetworkUtil(context).isNetworkAvailable()) {
            mOperationStatus.value = OperationStatus.Processing("processing", TaskEnum.ADD_DATA)
            val entryEntity = EntryEntityDataModel(
                "",
                code,
                owner,
                profession,
                area,
                housename,
                address,
                contact,
                whatsapp,
                mLoggedInUserEmail
            )
            if (validation(entryEntity)) {
                val job = CoroutineScope(IO).launch {
                    setDataToRepo(entryEntity)
                }
            }
        }
        else
        {
            mOperationStatus.value = OperationStatus.Failed(context.getString(R.string.no_network), TaskEnum.ADD_DATA)
        }
    }

    fun validation(
        entryEntity: EntryEntityDataModel
    ): Boolean {
        when {
            entryEntity.code.isEmpty() -> {
                mOperationStatus.value =
                    OperationStatus.ValidationFailed(
                        context.getString(R.string.code_missing),
                        TaskEnum.EDIT_DATA
                    )
                return false

            }
            entryEntity.owner.isEmpty() -> {
                mOperationStatus.value =
                    OperationStatus.ValidationFailed(
                        context.getString(R.string.owner_name_missing),
                        TaskEnum.EDIT_DATA
                    )
                return false

            }
            entryEntity.profession.isEmpty() -> {
                mOperationStatus.value =
                    OperationStatus.ValidationFailed(
                        context.getString(R.string.profession_missing),
                        TaskEnum.EDIT_DATA
                    )
                return false
            }
            entryEntity.housename.isEmpty() -> {
                mOperationStatus.value =
                    OperationStatus.ValidationFailed(
                        context.getString(R.string.house_name_missing),
                        TaskEnum.EDIT_DATA
                    )
                return false
            }


            else -> {

                return true

            }
        }
    }

    suspend fun setDataToRepo(entryEntity: EntryEntityDataModel) {
        val response = repository.addData(entryEntity)
        withContext(Dispatchers.Main)
        {
            mOperationStatus.value = OperationStatus.Success(context.getString(R.string.information_added), TaskEnum.ADD_DATA)
        }

    }


    fun getStatus(): LiveData<OperationStatus> = mOperationStatus
}