package net.appitiza.kuniyapally.viewmodels.main

import android.content.Context
import android.content.Intent
import android.graphics.*
import android.graphics.pdf.PdfDocument
import android.os.Environment
import android.util.Log
import android.view.View
import androidx.core.content.FileProvider
import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import net.appitiza.kuniyapally.R
import net.appitiza.kuniyapally.constants.Constants
import net.appitiza.kuniyapally.model.DisplayDataModel
import net.appitiza.kuniyapally.model.EntryEntityDataModel
import net.appitiza.kuniyapally.repositories.DataRepository
import net.appitiza.kuniyapally.ui.activities.*
import net.appitiza.kuniyapally.utils.*
import java.io.File
import java.io.FileOutputStream
import java.io.IOException


class MainViewModel(private val context: Context, private val repository: DataRepository) :
    ViewModel() {

    private var mLoggedInUserEmail by PreferenceHelper(Constants.PREF_LOGGEDIN_USER_EMAIL, "")
    private var mMutableLiveDataDisplayDataModel = MediatorLiveData<List<DisplayDataModel>>()
    private var mOperationStatus = MutableLiveData<OperationStatus>()
    fun moveToDetails(view: View, servercode: String) {

        val intent = Intent(context, DetailedActivity::class.java)
        intent.putExtra("servercode", servercode)
        context.startActivity(intent)


    }

    fun moveToAdd() {
        context.startActivity(Intent(context, AddActivity::class.java))

    }
    fun moveToEvent() {
        context.startActivity(Intent(context, EventActivity::class.java))

    }
    fun moveToGraph() {
        context.startActivity(Intent(context, GraphActivity::class.java))

    }
    fun moveToLogin() {
        context.startActivity(Intent(context, LoginActivity::class.java))
        (context as MainActivity).finish()
    }
    fun resetPassword() {
        if (mLoggedInUserEmail.isEmpty()) {
            mOperationStatus.value =
                OperationStatus.ValidationFailed(
                    context.getString(R.string.email_missing),
                    TaskEnum.RESET_PASSWORD
                )
        } else {
            mOperationStatus.value =
                OperationStatus.Processing("processing", TaskEnum.RESET_PASSWORD)

            val job = CoroutineScope(Dispatchers.IO).launch {

                if (repository.resetPassword(mLoggedInUserEmail)) {
                    withContext(Dispatchers.Main)
                    {
                        mOperationStatus.value =
                            OperationStatus.Success("Successful", TaskEnum.RESET_PASSWORD)
                    }
                } else {
                    withContext(Dispatchers.Main)
                    {
                        mOperationStatus.value =
                            OperationStatus.Failed("Failed", TaskEnum.RESET_PASSWORD)
                    }
                }

            }
        }

    }
    fun logout() {
        mOperationStatus.value =
            OperationStatus.Processing("processing", TaskEnum.LOGOUT)

        val job = CoroutineScope(Dispatchers.IO).launch {

            if (repository.logOut()) {
                withContext(Dispatchers.Main)
                {
                    mOperationStatus.value =
                        OperationStatus.Success("Successful", TaskEnum.LOGOUT)
                }
            } else {
                withContext(Dispatchers.Main)
                {
                    mOperationStatus.value =
                        OperationStatus.Failed("Failed", TaskEnum.LOGOUT)
                }
            }

        }

    }

    fun fetchData() {
        mOperationStatus.value = OperationStatus.Processing("processing", TaskEnum.FETCH_DATA)
        val job = CoroutineScope(Dispatchers.IO).launch {

            getDatalistFromRepo()

        }
    }

    fun searchData(fullName: String) {

        if (fullName.isNotEmpty()) {
            /* mOperationStatus.value =
                 OperationStatus.Processing("processing", TaskEnum.FETCH_DATA)*/

            CoroutineScope(Dispatchers.IO).launch {
                searchDatalistFromRepo(fullName)
            }
        } else {
            CoroutineScope(Dispatchers.IO).launch {
                getDatalistFromRepo()
            }
            /* mOperationStatus.value =
                 OperationStatus.ValidationFailed(
                     context.getString(R.string.owner_name_missing),
                     TaskEnum.SEARCH_DATA
                 )*/
        }
    }


    suspend fun getDatalistFromRepo() {
        val response = repository.getRemoteDataList()
        /*if (response == null || response.isEmpty()) {
            response = repository.getRemoteDataList()
        }*/
        withContext(Dispatchers.Main)
        {
            val entry = response.map { it.toEntry() }
            val display = entry.map { it.toDisplayEntry() }
            mMutableLiveDataDisplayDataModel.value = display
            withContext(Dispatchers.Main)
            {
                mOperationStatus.value = OperationStatus.Success("success", TaskEnum.FETCH_DATA)
            }
        }

    }

    suspend fun searchDatalistFromRepo(fullName: String) {
        val response = repository.search(fullName)

        withContext(Dispatchers.Main)
        {
            val entry = response.map { it.toEntry() }
            val display = entry.map { it.toDisplayEntry() }
            mMutableLiveDataDisplayDataModel.value = display
            withContext(Dispatchers.Main)
            {
                mOperationStatus.value = OperationStatus.Success("success", TaskEnum.FETCH_DATA)
            }
        }

    }


    fun delete(servercode: String) {
        if (NetworkUtil(context).isNetworkAvailable()) {
            mOperationStatus.value = OperationStatus.Processing("processing", TaskEnum.FETCH_DATA)
            val job = CoroutineScope(Dispatchers.IO).launch {
                deleteData(servercode)
            }
        } else {
            mOperationStatus.value =
                OperationStatus.Failed(context.getString(R.string.no_network), TaskEnum.FETCH_DATA)
        }

    }

    suspend fun deleteData(servercode: String) {

        val result = repository.deleteDataItem(servercode)
        withContext(Dispatchers.Main)
        {
            if (result) {
                val tempList =
                    mMutableLiveDataDisplayDataModel.value!!.filter { it.servercode == servercode }
                        .single()
                val datalist = mMutableLiveDataDisplayDataModel.value as ArrayList
                datalist.remove(tempList)
                mMutableLiveDataDisplayDataModel.value = datalist
                mOperationStatus.value = OperationStatus.Success("success", TaskEnum.FETCH_DATA)
            } else {
                mOperationStatus.value = OperationStatus.Failed("failed", TaskEnum.FETCH_DATA)
            }
        }
    }


    fun getEntryLiveData(): LiveData<List<DisplayDataModel>> = mMutableLiveDataDisplayDataModel
    fun getStatus(): LiveData<OperationStatus> = mOperationStatus


    fun generatePdf(fullName: String) {
        if (PermissionUtils(context).checkStoragePermission()) {
            mOperationStatus.value =
                OperationStatus.Processing("Generating PDF", TaskEnum.PDF_GENERATION)
            val job = CoroutineScope(Dispatchers.IO).launch {


                val response = if (fullName.isNotEmpty()) {
                    repository.search(fullName)
                } else {
                    repository.getLocalDataList()

                }
                val result = test(response)
                withContext(Dispatchers.Main)
                {
                    if (result) {
                        mOperationStatus.value =
                            OperationStatus.Success(
                                "PDF Generation Completed",
                                TaskEnum.PDF_GENERATION
                            )
                    } else {
                        mOperationStatus.value =
                            OperationStatus.Failed(
                                "PDF Generation Failed",
                                TaskEnum.PDF_GENERATION
                            )
                    }
                }
            }
        } else {
            PermissionUtils(context).requestStoragePermission(context as MainActivity)
        }

    }

    fun pdfGenerator(dataList: List<EntryEntityDataModel>): Boolean {

        // create a new document
        val document = PdfDocument()

        // crate a page description
        val pageInfo =
            PdfDocument.PageInfo.Builder(350, (dataList.size) * 250, (dataList.size) / 2).create()

        // start a page
        val page = document.startPage(pageInfo);

        val initialX = 20F
        var initialY = 40F
        val canvas: Canvas = page.canvas
        val paint = Paint()

        for (data in dataList) {
            // draw something on the page

            paint.color = Color.BLACK
            paint.typeface = Typeface.create(Typeface.DEFAULT, Typeface.BOLD)
            val bounds = Rect()
            paint.getTextBounds(data.owner, 0, (data.owner).length, bounds)
            initialY += bounds.height() + 20
            canvas.drawText(data.owner, initialX, initialY, paint)


            paint.color = Color.BLACK
            paint.typeface = Typeface.create(Typeface.DEFAULT, Typeface.NORMAL)

            paint.getTextBounds(
                context.getString(R.string.pdf_profession, data.profession),
                0,
                (context.getString(R.string.pdf_profession, data.profession)).length,
                bounds
            )
            initialY += bounds.height() + 10
            canvas.drawText(
                context.getString(R.string.pdf_profession, data.profession),
                initialX,
                initialY,
                paint
            )

            paint.getTextBounds(
                context.getString(R.string.pdf_nouse_name, data.housename),
                0,
                (context.getString(R.string.pdf_nouse_name, data.housename)).length,
                bounds
            )
            initialY += bounds.height() + 5
            canvas.drawText(
                context.getString(R.string.pdf_nouse_name, data.housename),
                initialX,
                initialY,
                paint
            )

            paint.getTextBounds(
                context.getString(R.string.pdf_address, data.address),
                0,
                (context.getString(R.string.pdf_address, data.address)).length,
                bounds
            )
            initialY += bounds.height() + 5
            canvas.drawText(
                context.getString(R.string.pdf_address, data.address),
                initialX,
                initialY,
                paint
            )

            paint.getTextBounds(
                context.getString(R.string.pdf_contact_whatsapp, data.contact, data.whatsapp),
                0,
                (context.getString(
                    R.string.pdf_contact_whatsapp,
                    data.contact,
                    data.whatsapp
                )).length,
                bounds
            )
            initialY += bounds.height() + 5
            canvas.drawText(
                context.getString(R.string.pdf_contact_whatsapp, data.contact, data.whatsapp),
                initialX,
                initialY,
                paint
            )

        }


        // finish the page
        document.finishPage(page)

        // write the document content
        val directory_path: String =
            context.getExternalFilesDir(Environment.DIRECTORY_DOCUMENTS)!!.absolutePath + "/Kuniyapally_Report/"
        val file = File(directory_path)
        if (!file.exists()) {
            file.mkdirs()
        }
        val targetPdf = directory_path + "Kuniyapally_Report.pdf"
        val filePath = File(targetPdf)
        // val filePath = file
        return try {
            document.writeTo(FileOutputStream(filePath))
            // close the document
            document.close()
            true
        } catch (e: IOException) {
            Log.e("main", "error $e")
            // close the document
            document.close()
            false
        }


    }

    fun test(dataList: List<EntryEntityDataModel>) : Boolean {
        var document = PdfDocument()

        var pageInfo = PdfDocument.PageInfo.Builder(400, 600, 5).create()
        var page = document.startPage(pageInfo);
        var canvas: Canvas = page.canvas
        var initialX = 20F
        var initialY = 40F
        for (data in dataList) {

           if(initialY > 500)
           {
               document.finishPage(page)
               page = document.startPage(pageInfo);

               canvas = page.canvas

               initialX = 20F
               initialY = 40F

           }


            val paint = Paint()
            paint.color = Color.BLACK
            paint.typeface = Typeface.create(Typeface.DEFAULT, Typeface.BOLD)
            val bounds = Rect()
            val code = data.code
            paint.getTextBounds(data.owner +"($code)", 0, (data.owner  +"($code)").length, bounds)
            initialY += bounds.height() + 20
            canvas.drawText(data.owner +"($code)", initialX, initialY, paint)


            paint.color = Color.BLACK
            paint.typeface = Typeface.create(Typeface.DEFAULT, Typeface.NORMAL)

            paint.getTextBounds(
                context.getString(R.string.pdf_profession, data.profession),
                0,
                (context.getString(R.string.pdf_profession, data.profession)).length,
                bounds
            )
            initialY += bounds.height() + 10
            canvas.drawText(
                context.getString(R.string.pdf_profession, data.profession),
                initialX,
                initialY,
                paint
            )

            paint.getTextBounds(
                context.getString(R.string.pdf_area, data.area),
                0,
                (context.getString(R.string.pdf_area, data.area)).length,
                bounds
            )
            initialY += bounds.height() + 10
            canvas.drawText(
                context.getString(R.string.pdf_area, data.area),
                initialX,
                initialY,
                paint
            )


            paint.getTextBounds(
                context.getString(R.string.pdf_nouse_name, data.housename),
                0,
                (context.getString(R.string.pdf_nouse_name, data.housename)).length,
                bounds
            )
            initialY += bounds.height() + 5
            canvas.drawText(
                context.getString(R.string.pdf_nouse_name, data.housename),
                initialX,
                initialY,
                paint
            )

            paint.getTextBounds(
                context.getString(R.string.pdf_address, data.address),
                0,
                (context.getString(R.string.pdf_address, data.address)).length,
                bounds
            )
            initialY += bounds.height() + 5
            canvas.drawText(
                context.getString(R.string.pdf_address, data.address),
                initialX,
                initialY,
                paint
            )

            paint.getTextBounds(
                context.getString(R.string.pdf_contact_whatsapp, data.contact, data.whatsapp),
                0,
                (context.getString(
                    R.string.pdf_contact_whatsapp,
                    data.contact,
                    data.whatsapp
                )).length,
                bounds
            )
            initialY += bounds.height() + 5
            canvas.drawText(
                context.getString(R.string.pdf_contact_whatsapp, data.contact, data.whatsapp),
                initialX,
                initialY,
                paint
            )



           if(data.entryid == dataList.last().entryid) {
               document.finishPage(page)
           }
        }

        val directory_path: String =
            context.getExternalFilesDir(Environment.DIRECTORY_DOCUMENTS)!!.absolutePath + "/Kuniyapally_Report/"
        val file = File(directory_path)
        if (!file.exists()) {
            file.mkdirs()
        }
        val targetPdf = directory_path + "Kuniyapally_Report.pdf"
        val filePath = File(targetPdf)
        return return try {
            document.writeTo(FileOutputStream(filePath))
            document.close()
            true
        } catch (e: IOException) {
            true
        }
    }

    fun openPDF() {

        val directory_path: String =
            context.getExternalFilesDir(Environment.DIRECTORY_DOCUMENTS)!!.absolutePath + "/Kuniyapally_Report/"
        val file = File(directory_path)
        if (!file.exists()) {
            file.mkdirs()
        }
        val targetPdf = directory_path + "Kuniyapally_Report.pdf"
        val filePath = File(targetPdf)


        val data =
            FileProvider.getUriForFile(
                context,
                "net.appitiza.kuniyapally.provider",
                filePath
            )
        context.grantUriPermission(
            context.packageName,
            data,
            Intent.FLAG_GRANT_READ_URI_PERMISSION
        )

        val mime = context.contentResolver.getType(data)
        val intent = Intent(Intent.ACTION_VIEW)
            .setDataAndType(data, mime)
            .addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
        context.startActivity(intent)
    }

    /*fun autoSaveReport()
    {
        val currentTime = System.currentTimeMillis()
        if((currentTime - mLastReportSaved) > (10 * 24 * 60  * 60 * 1000).toLong())
        {
            mLastReportSaved = currentTime
            selectFileLocation(fileName="$currentTime report.pdf")

        }
    }*/
}