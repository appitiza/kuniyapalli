package net.appitiza.kuniyapally.viewmodels.events

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import net.appitiza.kuniyapally.model.DisplayDataModel
import net.appitiza.kuniyapally.model.EntryEntityDataModel
import net.appitiza.kuniyapally.model.EventEntityDataModel
import net.appitiza.kuniyapally.repositories.DataRepository
import net.appitiza.kuniyapally.ui.activities.EventActivity
import net.appitiza.kuniyapally.utils.OperationStatus
import net.appitiza.kuniyapally.utils.TaskEnum


class EventsViewModel(private val context: Context, private val repository: DataRepository) :
    ViewModel() {
    private var mMutableLiveDataDisplayDataModel = MediatorLiveData<List<DisplayDataModel>>()
    private var mMutableLiveDataEventModel = MediatorLiveData<EventEntityDataModel>()
    private var mOperationStatus = MutableLiveData<OperationStatus>()


    fun fetchData() {
        mOperationStatus.value = OperationStatus.Processing("processing", TaskEnum.FETCH_DATA)
        val job = CoroutineScope(Dispatchers.IO).launch {

            getDatalistFromRepo()

        }
    }

    fun searchData(fullName: String) {

        if (fullName.isNotEmpty()) {
            /* mOperationStatus.value =
                 OperationStatus.Processing("processing", TaskEnum.FETCH_DATA)*/

            CoroutineScope(Dispatchers.IO).launch {
                searchDatalistFromRepo(fullName)
            }
        } else {
            CoroutineScope(Dispatchers.IO).launch {
                getDatalistFromRepo()
            }
            /* mOperationStatus.value =
                 OperationStatus.ValidationFailed(
                     context.getString(R.string.owner_name_missing),
                     TaskEnum.SEARCH_DATA
                 )*/
        }
    }

    fun reset()
    {
        CoroutineScope(Dispatchers.IO).launch {
            repository.deleteEventData()
            withContext(Dispatchers.Main)
            {
                (context as EventActivity).finish()
            }
        }
    }
    fun getEventStatus(userid:Int)
    {
        CoroutineScope(Dispatchers.IO).launch {
            val response = repository.getUserEvent(userid)
            withContext(Dispatchers.Main)
            {
                mMutableLiveDataEventModel.value = response
                withContext(Dispatchers.Main)
                {
                    mOperationStatus.value = OperationStatus.Success("success", TaskEnum.FETCH_EVENT)
                }
            }
        }
    }
    fun changeStatus(entry:String,userid:Int,eventId:Int = 0)
    {
        CoroutineScope(Dispatchers.IO).launch {
            repository.addEventData(EventEntityDataModel(eventId,entry,userid))
        }
    }

    suspend fun getDatalistFromRepo() {
        val response = repository.getLocalDataList()
        withContext(Dispatchers.Main)
        {

            val entry = response.map { it.toEntry() }
            val display = entry.map { it.toDisplayEntry() }
            mMutableLiveDataDisplayDataModel.value = display
            withContext(Dispatchers.Main)
            {
                mOperationStatus.value = OperationStatus.Success("success", TaskEnum.FETCH_DATA)
            }
        }

    }

    suspend fun searchDatalistFromRepo(fullName: String) {
        val response = repository.search(fullName)

        withContext(Dispatchers.Main)
        {
            val entry = response.map { it.toEntry() }
            val display = entry.map { it.toDisplayEntry() }
            mMutableLiveDataDisplayDataModel.value = display
            withContext(Dispatchers.Main)
            {
                mOperationStatus.value = OperationStatus.Success("success", TaskEnum.FETCH_DATA)
            }
        }

    }


    fun getEntryLiveData(): LiveData<List<DisplayDataModel>> = mMutableLiveDataDisplayDataModel
    fun getEventLiveData(): LiveData<EventEntityDataModel> = mMutableLiveDataEventModel
    fun getStatus(): LiveData<OperationStatus> = mOperationStatus

}