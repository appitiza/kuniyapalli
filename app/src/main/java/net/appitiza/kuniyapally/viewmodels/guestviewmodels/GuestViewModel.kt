package net.appitiza.kuniyapally.viewmodels.guestviewmodels

import android.content.Context
import android.content.Intent
import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import net.appitiza.kuniyapally.R
import net.appitiza.kuniyapally.constants.Constants
import net.appitiza.kuniyapally.model.DisplayDataModel
import net.appitiza.kuniyapally.repositories.DataRepository
import net.appitiza.kuniyapally.ui.activities.LoginAsGuestActivity
import net.appitiza.kuniyapally.ui.activities.MainActivity
import net.appitiza.kuniyapally.utils.NetworkUtil
import net.appitiza.kuniyapally.utils.OperationStatus
import net.appitiza.kuniyapally.utils.PreferenceHelper
import net.appitiza.kuniyapally.utils.TaskEnum

private const val SPLASH_TIMMER = 3000L

//private var mIsLoggedIn by PreferenceHelper(Constants.PREF_KEY_IS_USER_LOGGED_IN, false)
//private var mIsLanguageSet by PreferenceHelper(Constants.PREF_KEY_IS_LANGUAGE_SET, false)
private var mMutableLiveDataDisplayDataModel = MediatorLiveData<List<DisplayDataModel>>()
private var mOperationStatus = MutableLiveData<OperationStatus>()
private var mLoggedInAsAdmin by PreferenceHelper(Constants.PREF_LOGGEDIN_AS_AdMIN, false)
private var mLoggedInUserEmail by PreferenceHelper(Constants.PREF_LOGGEDIN_USER_EMAIL, "")

class GuestViewModel(private val context: Context, private val repository: DataRepository) :
    ViewModel() {
    private var mOperationStatus = MutableLiveData<OperationStatus>()


    fun login(phone: String) {
        if (NetworkUtil(context).isNetworkAvailable()) {
            if (phone.isEmpty()) {
                mOperationStatus.value =
                    OperationStatus.ValidationFailed(
                        context.getString(R.string.email_missing),
                        TaskEnum.EMAIL_LOGIN
                    )
            } else {
                mOperationStatus.value =
                    OperationStatus.Processing("processing", TaskEnum.GUEST_LOGIN)

                val job = CoroutineScope(Dispatchers.IO).launch {

                    if (repository.guestLogin(phone)) {
                        withContext(Dispatchers.Main)
                        {
                            mOperationStatus.value =
                                OperationStatus.Success("Successful", TaskEnum.GUEST_LOGIN)
                        }
                    } else {
                        withContext(Dispatchers.Main)
                        {
                            mOperationStatus.value =
                                OperationStatus.Failed("Login Failed", TaskEnum.GUEST_LOGIN)
                        }
                    }

                }
            }
        } else {
            mOperationStatus.value =
                OperationStatus.Failed(context.getString(R.string.no_network), TaskEnum.GUEST_LOGIN)
        }
    }



    fun continueAsGuest() {
        mLoggedInAsAdmin = false
        context.startActivity(Intent(context, MainActivity::class.java))
        (context as LoginAsGuestActivity).finish()
    }

    fun getEntryLiveData(): LiveData<List<DisplayDataModel>> = mMutableLiveDataDisplayDataModel
    fun getStatus(): LiveData<OperationStatus> = mOperationStatus
}