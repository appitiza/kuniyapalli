package net.appitiza.kuniyapally.viewmodels.detailed

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Build
import android.view.inputmethod.InputMethodManager
import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import net.appitiza.kuniyapally.model.EntryDataModel
import net.appitiza.kuniyapally.repositories.DataRepository
import net.appitiza.kuniyapally.ui.activities.DetailedActivity
import net.appitiza.kuniyapally.ui.activities.EditActivity
import net.appitiza.kuniyapally.utils.OperationStatus
import net.appitiza.kuniyapally.utils.PermissionUtils
import net.appitiza.kuniyapally.utils.TaskEnum

class DetailedViewModel(private val context: Context, private val repository: DataRepository) :
    ViewModel() {

    private var mMutableLiveDataDisplayDataModel = MediatorLiveData<EntryDataModel>()
    private var mOperationStatus = MutableLiveData<OperationStatus>()

    fun moveToEdit(servercode: String) {

        val intent = Intent(context, EditActivity::class.java)
        intent.putExtra("servercode", servercode)
        context.startActivity(intent)

    }

    fun hideSoftKey() {
        val mInputMethodeManager: InputMethodManager? =
            context.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        if (mInputMethodeManager != null && mInputMethodeManager.isActive) {
            if ((context as Activity).currentFocus != null) {
                mInputMethodeManager.hideSoftInputFromWindow(
                    (context).currentFocus!!.windowToken,
                    0
                )
            }

        }
    }

    fun getEntryDetails(serverCode: String) {
        mOperationStatus.value = OperationStatus.Processing("processing", TaskEnum.FETCH_DATA)
        val job = CoroutineScope(Dispatchers.IO).launch {
            getDetailsFromRepo(serverCode)
        }
    }

    suspend fun getDetailsFromRepo(serverCode: String) {
        val response = repository.getDataDetails(serverCode)
        withContext(Dispatchers.Main)
        {
            if (response != null) {
                val entry = response.toEntry()
                mMutableLiveDataDisplayDataModel.value = entry
                withContext(Dispatchers.Main)
                {
                    mOperationStatus.value = OperationStatus.Success("success", TaskEnum.FETCH_DATA)
                }
            }
            else
            {
                withContext(Dispatchers.Main)
                {
                    mOperationStatus.value = OperationStatus.Failed("success", TaskEnum.FETCH_DATA)
                }
            }
        }

    }

    fun loadCall(mobile: String) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

            if (PermissionUtils(context).checkCallPermission()) {
                //call
                openCallIntent(mobile)
            } else {
                PermissionUtils(context).requestCallPermission(context as DetailedActivity)

            }

        } else {
            //call
            openCallIntent(mobile)

        }
    }

    fun openCallIntent(telenumber: String) {
        val intent = Intent(Intent.ACTION_CALL)
        val contactNumber = StringBuilder()
        contactNumber.append("tel:").append(telenumber)
        intent.data = Uri.parse(contactNumber.toString())
        context.startActivity(intent)
    }

    fun openWatsAppIntent(telenumber: String) {
        val intent = Intent(Intent.ACTION_VIEW)
        intent.type = "text/plain"
        intent.data = Uri.parse("https://wa.me/$telenumber?text=")
        context.startActivity(intent)


    }

    fun getEntryLiveData(): LiveData<EntryDataModel> = mMutableLiveDataDisplayDataModel
    fun getStatus(): LiveData<OperationStatus> = mOperationStatus
}