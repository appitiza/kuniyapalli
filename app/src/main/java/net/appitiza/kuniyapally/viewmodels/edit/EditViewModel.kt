package net.appitiza.kuniyapally.viewmodels.edit

import android.content.Context
import android.os.Build
import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import net.appitiza.kuniyapally.R
import net.appitiza.kuniyapally.constants.Constants
import net.appitiza.kuniyapally.model.EntryDataModel
import net.appitiza.kuniyapally.model.EntryEntityDataModel
import net.appitiza.kuniyapally.repositories.DataRepository
import net.appitiza.kuniyapally.ui.activities.EditActivity
import net.appitiza.kuniyapally.utils.*

class EditViewModel(val context: Context, private val repository: DataRepository) :
    ViewModel() {
    private var mLoggedInUserEmail by PreferenceHelper(Constants.PREF_LOGGEDIN_USER_EMAIL, "")
    private var mMutableLiveDataDisplayDataModel = MediatorLiveData<EntryDataModel>()
    private var mOperationStatus = MutableLiveData<OperationStatus>()
    fun editData(
        entryID: Int,
        servercode: String,
        code: String,
        owner: String,
        profession: String,
        area: String,
        housename: String,
        address: String,
        contact: String,
        whatsapp: String
    ) {
        if(NetworkUtil(context).isNetworkAvailable()) {
            mOperationStatus.value = OperationStatus.Processing("processing", TaskEnum.EDIT_DATA)
            val entryEntity = EntryEntityDataModel(
                entryID,
                servercode,
                code,
                owner,
                profession,
                area,
                housename,
                address,
                contact,
                whatsapp,
                mLoggedInUserEmail
            )
            if (validation(entryEntity)) {
                val job = CoroutineScope(IO).launch {
                    editDataToRepo(entryEntity)
                }
            }
        }
        else
        {
            mOperationStatus.value = OperationStatus.Failed(context.getString(R.string.no_network), TaskEnum.ADD_DATA)
        }
    }

    fun validation(
        entryEntity: EntryEntityDataModel
    ): Boolean {
        when {
            entryEntity.code.isEmpty() -> {
                mOperationStatus.value =
                    OperationStatus.ValidationFailed(
                        context.getString(R.string.code_missing),
                        TaskEnum.EDIT_DATA
                    )
                return false

            }
            entryEntity.owner.isEmpty() -> {
                mOperationStatus.value =
                    OperationStatus.ValidationFailed(
                        context.getString(R.string.owner_name_missing),
                        TaskEnum.EDIT_DATA
                    )
                return false

            }
            entryEntity.profession.isEmpty() -> {
                mOperationStatus.value =
                    OperationStatus.ValidationFailed(
                        context.getString(R.string.owner_name_missing),
                        TaskEnum.EDIT_DATA
                    )
                return false
            }
            entryEntity.address.isEmpty() -> {
                mOperationStatus.value =
                    OperationStatus.ValidationFailed(
                        context.getString(R.string.owner_name_missing),
                        TaskEnum.EDIT_DATA
                    )
                return false
            }


            else -> {

                return true

            }
        }
    }

    suspend fun editDataToRepo(entryEntity: EntryEntityDataModel) {
        val response = repository.editData(entryEntity)
        withContext(Dispatchers.Main)
        {
            mOperationStatus.value = OperationStatus.Success(context.getString(R.string.information_edited), TaskEnum.EDIT_DATA)
        }

    }


    fun getEntryDetails(servercode: String) {
        mOperationStatus.value = OperationStatus.Processing("success", TaskEnum.FETCH_DATA)
        val job = CoroutineScope(Dispatchers.IO).launch {
            getDetailsFromRepo(servercode)
        }
    }

    suspend fun getDetailsFromRepo(servercode: String) {
        val response = repository.getDataDetails(servercode)
        withContext(Dispatchers.Main)
        {
            val entry = response.toEntry()
            mMutableLiveDataDisplayDataModel.value = entry
            withContext(Dispatchers.Main)
            {
                mOperationStatus.value = OperationStatus.Success("success", TaskEnum.FETCH_DATA)
            }
        }

    }


    fun getEntryLiveData(): LiveData<EntryDataModel> = mMutableLiveDataDisplayDataModel
    fun getStatus(): LiveData<OperationStatus> = mOperationStatus
}