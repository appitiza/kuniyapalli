package net.appitiza.kuniyapally.constants

class Constants {
    companion object {
        const val PREF_NAME = "KUNIAPALLY_PREFERENCE"
        const val PREF_IS_FIRST_LOGIN = "is_first_login"
        const val PREF_LAST_REPORT_SAVED = "last_report_saved"
        const val PREF_LAST_UPDATED_TIME = "last_updated_time"
        const val PREF_LOGGEDIN_USER_EMAIL = "logged_in_user_email"
        const val PREF_LOGGEDIN_USER_PASSWORD = "logged_in_user_password"
        const val PREF_LOGGEDIN_AS_AdMIN = "logged_in_as_admin"
        const val PREF_LOGGEDIN_AS_GUEST = "logged_in_as_guest"
        const val PREF_LOGGEDIN = "logged_in"

        const val entryid = "entryid"
        const val code = "code"
        const val owner = "owner"
        const val profession = "profession"
        const val area = "area"
        const val housename = "housename"
        const val address = "address"
        const val contact = "contact"
        const val whatsapp = "whatsapp"
        const val addedby = "addedby"
        const val syncDone = "syncDone"
        const val timestamp = "timestamp"
        const val localtimestamp = "localtimestamp"
        const val isDeleted = "is_deleted"


        const val ACTION_BROADCAST_EDIT = "ACTION_BROADCAST_EDIT"
        const val REQUEST_PERMISSIONSREQUEST_STORAGE_CODE = 1
        const val REQUEST_PERMISSIONSREQUEST_CALL_CODE = 2
        const val WRITE_REQUEST_CODE = 2


    }
}